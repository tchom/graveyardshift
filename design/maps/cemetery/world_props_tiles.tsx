<?xml version="1.0" encoding="UTF-8"?>
<tileset name="world_props_tiles" tilewidth="185" tileheight="320" tilecount="14" columns="0">
 <grid orientation="isometric" width="1" height="1"/>
 <tile id="11" type="stone">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="resourcesOnHit" value="1.stone,1.diamond|100"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="185" height="80" source="props/Forest_big_stone_01.png"/>
 </tile>
 <tile id="12" type="stone">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="resourcesOnHit" value="1.stone,1.diamond|100"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="109" height="105" source="props/Forest_big_stone_02.png"/>
 </tile>
 <tile id="13" type="tree">
  <properties>
   <property name="hitpoints" type="int" value="5"/>
   <property name="resourcesOnDestroy" value="5.wood|100"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="183" height="277" source="props/Forest_big_tree_leaf_01.png"/>
 </tile>
 <tile id="14" type="tree">
  <properties>
   <property name="hitpoints" type="int" value="5"/>
   <property name="resourcesOnDestroy" value="5.wood|100"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="150" height="320" source="props/Forest_big_tree_needle_04.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="58" height="78" source="props/Forest_small_grass_01.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="64" height="84" source="props/Forest_small_grass_02.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="64" height="77" source="props/Forest_small_grass_03.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="64" height="87" source="props/Forest_small_grass_04.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="53" height="68" source="props/Forest_small_grass_05.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="130" height="145" source="props/MHA_2D-Iso-Village_props_46.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="64" height="78" source="props/MHA_2D-Iso-Village_props_45.png"/>
 </tile>
 <tile id="27" type="tree">
  <properties>
   <property name="hitpoints" type="int" value="5"/>
   <property name="resourcesOnDestroy" value="5.wood|100"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="180" height="261" source="props/MHA_2D-Iso-Village_props_26.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="128" height="145" source="props/MHA_2D-Iso-Village_wall_stone_01.png"/>
 </tile>
 <tile id="29" type="basicgrave001">
  <properties>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="114" height="130" source="props/basic_grave_1.png"/>
 </tile>
</tileset>
