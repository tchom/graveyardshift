<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/tchom/Gamedev/graveyardshift/design/maps/cemetery/world_tiles.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>graveyardshift</string>
        <key>textureFileName</key>
        <filename>../../../dev/assets/tilesheets/world_tiles.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">BottomLeft</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../dev/assets/tilesheets/world_tiles.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">ground/ISO_Tile_Dirt_01.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPatch_01.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPatch_02.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPatch_03.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_3Cross_01.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_3Cross_02.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_3Cross_03.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_3Cross_04.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_4Cross.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Curve_01.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Curve_02.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Curve_03.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Curve_04.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_End_01.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_End_02.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_End_03.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_End_04.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Single.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Straight_01.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_GrassPath_Straight_02.png</key>
            <key type="filename">ground/ISO_Tile_Dirt_01_Grass_01.png</key>
            <key type="filename">ground/ISO_Tile_Water_01.png</key>
            <key type="filename">ground/ISO_Tile_Wood_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,41,74,82</rect>
                <key>scale9Paddings</key>
                <rect>37,41,74,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_big_stone_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,20,93,40</rect>
                <key>scale9Paddings</key>
                <rect>46,20,93,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_big_stone_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,26,93,53</rect>
                <key>scale9Paddings</key>
                <rect>46,26,93,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_big_tree_leaf_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,65,93,130</rect>
                <key>scale9Paddings</key>
                <rect>46,65,93,130</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_big_tree_needle_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,72,93,145</rect>
                <key>scale9Paddings</key>
                <rect>46,72,93,145</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_small_grass_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,12,29,23</rect>
                <key>scale9Paddings</key>
                <rect>15,12,29,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_small_grass_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,13,32,26</rect>
                <key>scale9Paddings</key>
                <rect>16,13,32,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_small_grass_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,11,32,23</rect>
                <key>scale9Paddings</key>
                <rect>16,11,32,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_small_grass_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,14,32,28</rect>
                <key>scale9Paddings</key>
                <rect>16,14,32,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/Forest_small_grass_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,9,27,18</rect>
                <key>scale9Paddings</key>
                <rect>13,9,27,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/MHA_2D-Iso-Village_props_26.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,65,90,131</rect>
                <key>scale9Paddings</key>
                <rect>45,65,90,131</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/MHA_2D-Iso-Village_props_45.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,31,50,61</rect>
                <key>scale9Paddings</key>
                <rect>25,31,50,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/MHA_2D-Iso-Village_props_46.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,36,65,73</rect>
                <key>scale9Paddings</key>
                <rect>33,36,65,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/MHA_2D-Iso-Village_wall_stone_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,36,64,73</rect>
                <key>scale9Paddings</key>
                <rect>32,36,64,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">props/basic_grave_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,27,44,55</rect>
                <key>scale9Paddings</key>
                <rect>22,27,44,55</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>ground</filename>
            <filename>props</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties">
            <key>plain::bool-property</key>
            <struct type="ExporterProperty">
                <key>value</key>
                <string>false</string>
            </struct>
            <key>plain::string-property</key>
            <struct type="ExporterProperty">
                <key>value</key>
                <string>hello world</string>
            </struct>
        </map>
    </struct>
</data>
