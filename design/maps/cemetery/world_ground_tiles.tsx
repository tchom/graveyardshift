<?xml version="1.0" encoding="UTF-8"?>
<tileset name="world_ground_tiles" tilewidth="148" tileheight="164" tilecount="23" columns="0">
 <tileoffset x="-9" y="100"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="diggable" type="bool" value="true"/>
   <property name="hitpoints" type="int" value="10"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_Grass_01.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="heightOffset" type="int" value="2"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Water_01.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPatch_01.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPatch_02.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPatch_03.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_01.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_02.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_03.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_04.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_4Cross.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Curve_01.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Curve_02.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Curve_03.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Curve_04.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_End_01.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_End_02.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_End_03.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_End_04.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Single.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Straight_01.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01_GrassPath_Straight_02.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="diggable" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="true"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Wood_01.png"/>
 </tile>
 <tile id="22" type="dirt">
  <properties>
   <property name="diggable" type="bool" value="true"/>
   <property name="hitpoints" type="int" value="0"/>
   <property name="walkable" type="bool" value="false"/>
  </properties>
  <image width="148" height="164" source="ground/ISO_Tile_Dirt_01.png"/>
 </tile>
</tileset>
