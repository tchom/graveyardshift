--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:8e6178d0b501b4ad5b25adf78618fe80:983fff02c13c8f72b5608d29a2aff422:7a1021bc0125d4feb77da013bd222840$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- buttons/closebutton_down
            name="buttons/closebutton_down",
            x=233,
            y=946,
            width=20,
            height=19,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 19
        },
        {
            -- buttons/closebutton_over
            name="buttons/closebutton_over",
            x=377,
            y=937,
            width=28,
            height=28,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 28,
            sourceHeight = 28
        },
        {
            -- buttons/closebutton_up
            name="buttons/closebutton_up",
            x=407,
            y=937,
            width=28,
            height=28,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 28,
            sourceHeight = 28
        },
        {
            -- cursor_interactionmode
            name="cursor_interactionmode",
            x=386,
            y=755,
            width=41,
            height=43,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 41,
            sourceHeight = 43
        },
        {
            -- cursor_selectionmode
            name="cursor_selectionmode",
            x=104,
            y=904,
            width=64,
            height=64,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 64,
            sourceHeight = 64
        },
        {
            -- energy_bar
            name="energy_bar",
            x=277,
            y=643,
            width=259,
            height=16,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 259,
            sourceHeight = 16
        },
        {
            -- energy_bar_inner
            name="energy_bar_inner",
            x=277,
            y=679,
            width=249,
            height=12,
            sourceX = 1,
            sourceY = 0,
            sourceWidth = 259,
            sourceHeight = 16
        },
        {
            -- icon_backing
            name="icon_backing",
            x=104,
            y=814,
            width=72,
            height=88,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 72,
            sourceHeight = 88
        },
        {
            -- icons/icon_axe
            name="icons/icon_axe",
            x=437,
            y=707,
            width=50,
            height=44,
            sourceX = 0,
            sourceY = 2,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_basicbush
            name="icons/icon_basicbush",
            x=489,
            y=747,
            width=46,
            height=44,
            sourceX = 2,
            sourceY = 3,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_basicgrave001
            name="icons/icon_basicgrave001",
            x=342,
            y=789,
            width=42,
            height=48,
            sourceX = 3,
            sourceY = 0,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_decorationgroup
            name="icons/icon_decorationgroup",
            x=437,
            y=753,
            width=46,
            height=44,
            sourceX = 2,
            sourceY = 3,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_gravegroup
            name="icons/icon_gravegroup",
            x=342,
            y=839,
            width=42,
            height=48,
            sourceX = 3,
            sourceY = 0,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_interact
            name="icons/icon_interact",
            x=359,
            y=707,
            width=44,
            height=46,
            sourceX = 3,
            sourceY = 3,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_pick
            name="icons/icon_pick",
            x=377,
            y=889,
            width=46,
            height=46,
            sourceX = 1,
            sourceY = 1,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_scythe
            name="icons/icon_scythe",
            x=233,
            y=896,
            width=48,
            height=48,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 48,
            sourceHeight = 48
        },
        {
            -- icons/icon_shovel
            name="icons/icon_shovel",
            x=283,
            y=896,
            width=46,
            height=48,
            sourceX = 2,
            sourceY = 1,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_toolsgroup
            name="icons/icon_toolsgroup",
            x=489,
            y=707,
            width=50,
            height=38,
            sourceX = 0,
            sourceY = 6,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- icons/icon_wateringcan
            name="icons/icon_wateringcan",
            x=331,
            y=896,
            width=44,
            height=48,
            sourceX = 3,
            sourceY = 0,
            sourceWidth = 50,
            sourceHeight = 50
        },
        {
            -- inventory_backing
            name="inventory_backing",
            x=1,
            y=1,
            width=538,
            height=640,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 538,
            sourceHeight = 640
        },
        {
            -- money_bar
            name="money_bar",
            x=277,
            y=661,
            width=259,
            height=16,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 259,
            sourceHeight = 16
        },
        {
            -- money_bar_inner
            name="money_bar_inner",
            x=277,
            y=693,
            width=249,
            height=12,
            sourceX = 10,
            sourceY = 1,
            sourceWidth = 259,
            sourceHeight = 16
        },
        {
            -- slot_empty
            name="slot_empty",
            x=178,
            y=814,
            width=80,
            height=80,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 80
        },
        {
            -- slot_highlight
            name="slot_highlight",
            x=260,
            y=814,
            width=80,
            height=80,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 80
        },
        {
            -- slot_locked
            name="slot_locked",
            x=170,
            y=904,
            width=61,
            height=61,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 61,
            sourceHeight = 61
        },
        {
            -- slot_selected
            name="slot_selected",
            x=277,
            y=707,
            width=80,
            height=80,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 80
        },
        {
            -- time_circle
            name="time_circle",
            x=104,
            y=643,
            width=171,
            height=169,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 171,
            sourceHeight = 169
        },
        {
            -- toolbar_backing
            name="toolbar_backing",
            x=1,
            y=643,
            width=101,
            height=325,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 101,
            sourceHeight = 325
        },
    },
    
    sheetContentWidth = 540,
    sheetContentHeight = 969
}

SheetInfo.frameIndex =
{

    ["buttons/closebutton_down"] = 1,
    ["buttons/closebutton_over"] = 2,
    ["buttons/closebutton_up"] = 3,
    ["cursor_interactionmode"] = 4,
    ["cursor_selectionmode"] = 5,
    ["energy_bar"] = 6,
    ["energy_bar_inner"] = 7,
    ["icon_backing"] = 8,
    ["icons/icon_axe"] = 9,
    ["icons/icon_basicbush"] = 10,
    ["icons/icon_basicgrave001"] = 11,
    ["icons/icon_decorationgroup"] = 12,
    ["icons/icon_gravegroup"] = 13,
    ["icons/icon_interact"] = 14,
    ["icons/icon_pick"] = 15,
    ["icons/icon_scythe"] = 16,
    ["icons/icon_shovel"] = 17,
    ["icons/icon_toolsgroup"] = 18,
    ["icons/icon_wateringcan"] = 19,
    ["inventory_backing"] = 20,
    ["money_bar"] = 21,
    ["money_bar_inner"] = 22,
    ["slot_empty"] = 23,
    ["slot_highlight"] = 24,
    ["slot_locked"] = 25,
    ["slot_selected"] = 26,
    ["time_circle"] = 27,
    ["toolbar_backing"] = 28,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
