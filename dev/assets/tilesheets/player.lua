--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:d5055cd821a11b44e41d1d7f503c8e87:409759d170ca5d22686088c92978aeef:b2f580ea7c37465eac371f095cbaf8c7$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- idle/alienGreen_stand
            name="idle/alienGreen_stand",
            x=143,
            y=1,
            width=66,
            height=92,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 66,
            sourceHeight = 92
        },
        {
            -- walk/alienGreen_walk1
            name="walk/alienGreen_walk1",
            x=73,
            y=1,
            width=68,
            height=93,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 68,
            sourceHeight = 93
        },
        {
            -- walk/alienGreen_walk2
            name="walk/alienGreen_walk2",
            x=1,
            y=1,
            width=70,
            height=96,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 70,
            sourceHeight = 96
        },
    },
    
    sheetContentWidth = 210,
    sheetContentHeight = 98
}

SheetInfo.frameIndex =
{

    ["idle/alienGreen_stand"] = 1,
    ["walk/alienGreen_walk1"] = 2,
    ["walk/alienGreen_walk2"] = 3,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
