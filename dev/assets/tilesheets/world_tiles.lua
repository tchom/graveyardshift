--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:9071f8c1e1be0d0e2d978aaf8559ebf6:65d7ec9b7dc81f897e5a185d91dd67c3:9b4659b6a4a56de17284ca7e998da882$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- ground/ISO_Tile_Dirt_01
            name="ground/ISO_Tile_Dirt_01",
            x=263,
            y=83,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_Grass_01
            name="ground/ISO_Tile_Dirt_01_Grass_01",
            x=601,
            y=631,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPatch_01
            name="ground/ISO_Tile_Dirt_01_GrassPatch_01",
            x=413,
            y=90,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPatch_02
            name="ground/ISO_Tile_Dirt_01_GrassPatch_02",
            x=563,
            y=133,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPatch_03
            name="ground/ISO_Tile_Dirt_01_GrassPatch_03",
            x=713,
            y=133,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_3Cross_01
            name="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_01",
            x=1,
            y=230,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_3Cross_02
            name="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_02",
            x=151,
            y=249,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_3Cross_03
            name="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_03",
            x=301,
            y=256,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_3Cross_04
            name="ground/ISO_Tile_Dirt_01_GrassPath_3Cross_04",
            x=451,
            y=299,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_4Cross
            name="ground/ISO_Tile_Dirt_01_GrassPath_4Cross",
            x=601,
            y=299,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Curve_01
            name="ground/ISO_Tile_Dirt_01_GrassPath_Curve_01",
            x=751,
            y=299,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Curve_02
            name="ground/ISO_Tile_Dirt_01_GrassPath_Curve_02",
            x=1,
            y=396,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Curve_03
            name="ground/ISO_Tile_Dirt_01_GrassPath_Curve_03",
            x=151,
            y=415,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Curve_04
            name="ground/ISO_Tile_Dirt_01_GrassPath_Curve_04",
            x=301,
            y=422,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_End_01
            name="ground/ISO_Tile_Dirt_01_GrassPath_End_01",
            x=451,
            y=465,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_End_02
            name="ground/ISO_Tile_Dirt_01_GrassPath_End_02",
            x=601,
            y=465,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_End_03
            name="ground/ISO_Tile_Dirt_01_GrassPath_End_03",
            x=751,
            y=465,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_End_04
            name="ground/ISO_Tile_Dirt_01_GrassPath_End_04",
            x=1,
            y=562,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Single
            name="ground/ISO_Tile_Dirt_01_GrassPath_Single",
            x=151,
            y=581,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Straight_01
            name="ground/ISO_Tile_Dirt_01_GrassPath_Straight_01",
            x=301,
            y=588,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Dirt_01_GrassPath_Straight_02
            name="ground/ISO_Tile_Dirt_01_GrassPath_Straight_02",
            x=451,
            y=631,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Water_01
            name="ground/ISO_Tile_Water_01",
            x=751,
            y=631,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- ground/ISO_Tile_Wood_01
            name="ground/ISO_Tile_Wood_01",
            x=1,
            y=728,
            width=148,
            height=164,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 148,
            sourceHeight = 164
        },
        {
            -- props/basic_grave_1
            name="props/basic_grave_1",
            x=678,
            y=1,
            width=114,
            height=130,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 114,
            sourceHeight = 130
        },
        {
            -- props/Forest_big_stone_01
            name="props/Forest_big_stone_01",
            x=248,
            y=1,
            width=185,
            height=80,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 185,
            sourceHeight = 80
        },
        {
            -- props/Forest_big_stone_02
            name="props/Forest_big_stone_02",
            x=567,
            y=1,
            width=109,
            height=105,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 109,
            sourceHeight = 105
        },
        {
            -- props/Forest_big_tree_leaf_01
            name="props/Forest_big_tree_leaf_01",
            x=333,
            y=797,
            width=183,
            height=277,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 183,
            sourceHeight = 277
        },
        {
            -- props/Forest_big_tree_needle_04
            name="props/Forest_big_tree_needle_04",
            x=518,
            y=797,
            width=150,
            height=320,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 150,
            sourceHeight = 320
        },
        {
            -- props/Forest_small_grass_01
            name="props/Forest_small_grass_01",
            x=122,
            y=1,
            width=58,
            height=78,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 58,
            sourceHeight = 78
        },
        {
            -- props/Forest_small_grass_02
            name="props/Forest_small_grass_02",
            x=435,
            y=1,
            width=64,
            height=84,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 64,
            sourceHeight = 84
        },
        {
            -- props/Forest_small_grass_03
            name="props/Forest_small_grass_03",
            x=56,
            y=1,
            width=64,
            height=77,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 64,
            sourceHeight = 77
        },
        {
            -- props/Forest_small_grass_04
            name="props/Forest_small_grass_04",
            x=501,
            y=1,
            width=64,
            height=87,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 64,
            sourceHeight = 87
        },
        {
            -- props/Forest_small_grass_05
            name="props/Forest_small_grass_05",
            x=1,
            y=1,
            width=53,
            height=68,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 53,
            sourceHeight = 68
        },
        {
            -- props/MHA_2D-Iso-Village_props_26
            name="props/MHA_2D-Iso-Village_props_26",
            x=151,
            y=754,
            width=180,
            height=261,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 180,
            sourceHeight = 261
        },
        {
            -- props/MHA_2D-Iso-Village_props_45
            name="props/MHA_2D-Iso-Village_props_45",
            x=182,
            y=1,
            width=64,
            height=78,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 64,
            sourceHeight = 78
        },
        {
            -- props/MHA_2D-Iso-Village_props_46
            name="props/MHA_2D-Iso-Village_props_46",
            x=1,
            y=81,
            width=130,
            height=145,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 130,
            sourceHeight = 145
        },
        {
            -- props/MHA_2D-Iso-Village_wall_stone_01
            name="props/MHA_2D-Iso-Village_wall_stone_01",
            x=133,
            y=83,
            width=128,
            height=145,
            sourceX = 0,
            sourceY = 0,
            sourceWidth = 128,
            sourceHeight = 145
        },
    },
    
    sheetContentWidth = 900,
    sheetContentHeight = 1118
}

SheetInfo.frameIndex =
{

    ["ground/ISO_Tile_Dirt_01"] = 1,
    ["ground/ISO_Tile_Dirt_01_Grass_01"] = 2,
    ["ground/ISO_Tile_Dirt_01_GrassPatch_01"] = 3,
    ["ground/ISO_Tile_Dirt_01_GrassPatch_02"] = 4,
    ["ground/ISO_Tile_Dirt_01_GrassPatch_03"] = 5,
    ["ground/ISO_Tile_Dirt_01_GrassPath_3Cross_01"] = 6,
    ["ground/ISO_Tile_Dirt_01_GrassPath_3Cross_02"] = 7,
    ["ground/ISO_Tile_Dirt_01_GrassPath_3Cross_03"] = 8,
    ["ground/ISO_Tile_Dirt_01_GrassPath_3Cross_04"] = 9,
    ["ground/ISO_Tile_Dirt_01_GrassPath_4Cross"] = 10,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Curve_01"] = 11,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Curve_02"] = 12,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Curve_03"] = 13,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Curve_04"] = 14,
    ["ground/ISO_Tile_Dirt_01_GrassPath_End_01"] = 15,
    ["ground/ISO_Tile_Dirt_01_GrassPath_End_02"] = 16,
    ["ground/ISO_Tile_Dirt_01_GrassPath_End_03"] = 17,
    ["ground/ISO_Tile_Dirt_01_GrassPath_End_04"] = 18,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Single"] = 19,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Straight_01"] = 20,
    ["ground/ISO_Tile_Dirt_01_GrassPath_Straight_02"] = 21,
    ["ground/ISO_Tile_Water_01"] = 22,
    ["ground/ISO_Tile_Wood_01"] = 23,
    ["props/basic_grave_1"] = 24,
    ["props/Forest_big_stone_01"] = 25,
    ["props/Forest_big_stone_02"] = 26,
    ["props/Forest_big_tree_leaf_01"] = 27,
    ["props/Forest_big_tree_needle_04"] = 28,
    ["props/Forest_small_grass_01"] = 29,
    ["props/Forest_small_grass_02"] = 30,
    ["props/Forest_small_grass_03"] = 31,
    ["props/Forest_small_grass_04"] = 32,
    ["props/Forest_small_grass_05"] = 33,
    ["props/MHA_2D-Iso-Village_props_26"] = 34,
    ["props/MHA_2D-Iso-Village_props_45"] = 35,
    ["props/MHA_2D-Iso-Village_props_46"] = 36,
    ["props/MHA_2D-Iso-Village_wall_stone_01"] = 37,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
