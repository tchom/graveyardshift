return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.0.3",
  orientation = "isometric",
  renderorder = "right-down",
  width = 16,
  height = 16,
  tilewidth = 128,
  tileheight = 64,
  nextobjectid = 7,
  properties = {},
  tilesets = {
    {
      name = "world_ground_tiles",
      firstgid = 1,
      filename = "../../../design/maps/cemetery/world_ground_tiles.tsx",
      tilewidth = 148,
      tileheight = 164,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = -9,
        y = 100
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 23,
      tiles = {
        {
          id = 0,
          properties = {
            ["diggable"] = true,
            ["hitpoints"] = 10,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_Grass_01.png",
          width = 148,
          height = 164
        },
        {
          id = 1,
          properties = {
            ["diggable"] = false,
            ["heightOffset"] = 2,
            ["hitpoints"] = 0,
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Water_01.png",
          width = 148,
          height = 164
        },
        {
          id = 2,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPatch_01.png",
          width = 148,
          height = 164
        },
        {
          id = 3,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPatch_02.png",
          width = 148,
          height = 164
        },
        {
          id = 4,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPatch_03.png",
          width = 148,
          height = 164
        },
        {
          id = 5,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_3Cross_01.png",
          width = 148,
          height = 164
        },
        {
          id = 6,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_3Cross_02.png",
          width = 148,
          height = 164
        },
        {
          id = 7,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_3Cross_03.png",
          width = 148,
          height = 164
        },
        {
          id = 8,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_3Cross_04.png",
          width = 148,
          height = 164
        },
        {
          id = 9,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_4Cross.png",
          width = 148,
          height = 164
        },
        {
          id = 10,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Curve_01.png",
          width = 148,
          height = 164
        },
        {
          id = 11,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Curve_02.png",
          width = 148,
          height = 164
        },
        {
          id = 12,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Curve_03.png",
          width = 148,
          height = 164
        },
        {
          id = 13,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Curve_04.png",
          width = 148,
          height = 164
        },
        {
          id = 14,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_End_01.png",
          width = 148,
          height = 164
        },
        {
          id = 15,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_End_02.png",
          width = 148,
          height = 164
        },
        {
          id = 16,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_End_03.png",
          width = 148,
          height = 164
        },
        {
          id = 17,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_End_04.png",
          width = 148,
          height = 164
        },
        {
          id = 18,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Single.png",
          width = 148,
          height = 164
        },
        {
          id = 19,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Straight_01.png",
          width = 148,
          height = 164
        },
        {
          id = 20,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01_GrassPath_Straight_02.png",
          width = 148,
          height = 164
        },
        {
          id = 21,
          properties = {
            ["diggable"] = false,
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Wood_01.png",
          width = 148,
          height = 164
        },
        {
          id = 22,
          type = "dirt",
          properties = {
            ["diggable"] = true,
            ["hitpoints"] = 0,
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/ground/ISO_Tile_Dirt_01.png",
          width = 148,
          height = 164
        }
      }
    },
    {
      name = "world_props_tiles",
      firstgid = 24,
      filename = "../../../design/maps/cemetery/world_props_tiles.tsx",
      tilewidth = 185,
      tileheight = 320,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "isometric",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 14,
      tiles = {
        {
          id = 11,
          type = "stone",
          properties = {
            ["hitpoints"] = 0,
            ["resourcesOnHit"] = "1.stone,1.diamond|100",
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/Forest_big_stone_01.png",
          width = 185,
          height = 80
        },
        {
          id = 12,
          type = "stone",
          properties = {
            ["hitpoints"] = 0,
            ["resourcesOnHit"] = "1.stone,1.diamond|100",
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/Forest_big_stone_02.png",
          width = 109,
          height = 105
        },
        {
          id = 13,
          type = "tree",
          properties = {
            ["hitpoints"] = 5,
            ["resourcesOnDestroy"] = "5.wood|100",
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/Forest_big_tree_leaf_01.png",
          width = 183,
          height = 277
        },
        {
          id = 14,
          type = "tree",
          properties = {
            ["hitpoints"] = 5,
            ["resourcesOnDestroy"] = "5.wood|100",
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/Forest_big_tree_needle_04.png",
          width = 150,
          height = 320
        },
        {
          id = 15,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/props/Forest_small_grass_01.png",
          width = 58,
          height = 78
        },
        {
          id = 16,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/props/Forest_small_grass_02.png",
          width = 64,
          height = 84
        },
        {
          id = 17,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/props/Forest_small_grass_03.png",
          width = 64,
          height = 77
        },
        {
          id = 18,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/props/Forest_small_grass_04.png",
          width = 64,
          height = 87
        },
        {
          id = 19,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = true
          },
          image = "../../../design/maps/cemetery/props/Forest_small_grass_05.png",
          width = 53,
          height = 68
        },
        {
          id = 23,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/MHA_2D-Iso-Village_props_46.png",
          width = 130,
          height = 145
        },
        {
          id = 25,
          properties = {
            ["hitpoints"] = 0,
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/MHA_2D-Iso-Village_props_45.png",
          width = 64,
          height = 78
        },
        {
          id = 27,
          type = "tree",
          properties = {
            ["hitpoints"] = 5,
            ["resourcesOnDestroy"] = "5.wood|100",
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/MHA_2D-Iso-Village_props_26.png",
          width = 180,
          height = 261
        },
        {
          id = 28,
          properties = {
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/MHA_2D-Iso-Village_wall_stone_01.png",
          width = 128,
          height = 145
        },
        {
          id = 29,
          type = "basicgrave001",
          properties = {
            ["walkable"] = false
          },
          image = "../../../design/maps/cemetery/props/basic_grave_1.png",
          width = 114,
          height = 130
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "ground_tiles",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 1, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 22, 22, 22, 22, 22, 22, 22, 22, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "props_tiles",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 52, 52, 52, 52, 52, 52, 52, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "portals",
      visible = true,
      opacity = 1,
      offsetx = -6,
      offsety = -8,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 384,
          width = 64,
          height = 64,
          rotation = 0,
          visible = true,
          properties = {
            ["default"] = true,
            ["out"] = false,
            ["target"] = "cemetery",
            ["trigger_adjacent"] = false
          }
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 384,
          width = 64,
          height = 64,
          rotation = 0,
          visible = true,
          properties = {
            ["default"] = false,
            ["out"] = true,
            ["target"] = "cemetery",
            ["trigger_adjacent"] = false
          }
        }
      }
    }
  }
}
