return {
	buttons = {
		{
			icon =	"icons/icon_interact",
			command = "set_active_item",
			commandParams = {
				"interact"
			}

		},
		{
			icon =	"icons/icon_shovel",
			command = "set_active_item",
			commandParams = {
				"shovel01"
			}

		},
		{
			icon =	"icons/icon_axe",
			command = "set_active_item",
			commandParams = {
				"axe01"
			}

		},
		{
			icon =	"icons/icon_pick",
			command = "set_active_item",
			commandParams = {
				"pick01"
			}

		},
		{
			icon =	"icons/icon_gravegroup",
			command = "send_ui_signal",
			commandParams = {
				"category_selection_panel",
				"open",
				{"graves", "Build Grave"}
			}

		}
	}
}
