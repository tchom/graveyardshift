class = require 'lib/middleclass'
Stateful = require 'lib/stateful'
GameSettings = require 'data/gamesettings'


require 'core/game'

_G.Game = {
    Command = require "core/commands/commandmanager":new(),
    Model = require("core/model/model"):new(),
    GameState = Game:new(),
    AssetsManager = require "core/assets/assetsmanager":new(),

  }

local game

function love.load()
  -- Launch Game starting at Main Menu
	game = _G.Game.GameState
end

function love.update(dt)
  game:update(dt)
end

function love.draw()
  game:draw()
end

function love.keypressed(key, code)
  game:keypressed(key, code)

  --Debug
	if key == "`" then --set to whatever key you want to use
		--debug.debug()
	end
end

function love.mousepressed(x, y, button, istouch)
  game:mousepressed(x, y, button, istouch)
end

function love.mousereleased(x, y, button, istouch)
  game:mousereleased(x, y, button, istouch)
end

-- DEBUG
io.stdout:setvbuf("no")