--- game.lua

Game = class('Game'):include(Stateful)

require 'core/states/menustate'
require 'core/states/gameloadstate'
require 'core/states/gamescenestate'
require 'core/states/pausestate'
require 'core/states/gameoverstate'

function Game:initialize()
	self.test = "Test"
  --self:gotoState('Menu')
  self:gotoState('Menu')
end

function Game:exit()
end

function Game:update(dt)
end

function Game:draw()
end

function Game:keypressed(key, code)
  -- Pause game
  if key == 'space' then
    self:pushState('GameScene')
  end
end

function Game:mousepressed(x, y, button, isTouch)
end

function Game:mousereleased(x, y, button, isTouch)
end