return{
	new = function()

		local model = {
			modules = {}
		}

		function model:addModule(name, module)
			self.modules[name] = module
		end

		function model:getModule(name)
			assert(self.modules[name])
			return self.modules[name]
		end

		function model:update(dt)
			for key, module in pairs(self.modules) do 
				module:update(dt) 
			end
		end


		return model
	end
}