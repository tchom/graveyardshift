local Vec2 = require "core/geom/vec2"
return {
	new = function(_posX, _posZ)
		local t = {
			positionGroup = {},
			graveItem = _graveItem,
			graveDepth = 0
		}

		table.insert( t.positionGroup,Vec2.new(_posX, _posZ))
		table.insert( t.positionGroup,Vec2.new(_posX+1, _posZ))

		function t:hit(_posX, _posZ)
			local isHit = false
			for i = 1, #self.positionGroup do
				if self.positionGroup[i].x == _posX and self.positionGroup[i].y == _posZ then 
					isHit = true 
				end
			end
			return isHit
		end

		function t:lowerDepth(value)
			t.graveDepth = math.min( t.graveDepth + value, 5 )
		end

		function t:raiseDepth(value)
			t.graveDepth = math.max( t.graveDepth + value, 0 )
		end

		function t:atMaxDepth()
			return self.graveDepth == 5
		end


		return t
	end
}