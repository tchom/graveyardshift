local Waypoint = require "core/model/waypoint"
local GeomUtils = require "core/geom/geom_helper"

return{
	createConnectorWaypoint = function(self, map, posX, posY, isDirty)
		local targetWaypoint = Waypoint.new(posX, posY)
		local targetMapChunk = map:getChunkByTile(posX, posY)

		self:connectWaypoint(map, targetWaypoint, targetMapChunk)
		targetWaypoint.dirty = isDirty or false

		return targetWaypoint
	end,


	connectWaypoint = function(self, map, targetWaypoint, targetMapChunk)
		for j = targetMapChunk.posZ - 1, targetMapChunk.posZ + 1 do
			for i = targetMapChunk.posX - 1, targetMapChunk.posX + 1 do
				if (map:chunkIsLegal(i, j)) then
					self:createConnections(map:getChunk(i, j), targetWaypoint, map)
				end
			end
		end
	end,

	createConnections = function(self, mapChunk, waypoint, map)
		for i = 1, #mapChunk.waypoints do
			local potentialConnectedWaypoint = mapChunk.waypoints[i]
			local path = GeomUtils.calculateBreshamLine(waypoint.x, waypoint.z, potentialConnectedWaypoint.x, potentialConnectedWaypoint.z)
			if(GeomUtils.checkValidPath(path, map)) then
				waypoint:addConnection(potentialConnectedWaypoint)
				potentialConnectedWaypoint:addConnection(waypoint)
			end
		end
	end,



	 isWaypoint = function(self, x, z, map)
		local isWaypoint = false

        if (self.isValidNorthEast(x, z, map) or self.isValidNorthWest(x, z, map) or self.isValidSouthEast(x, z, map) or self.isValidSouthWest(x, z, map)) then
            isWaypoint = true
        end
        return isWaypoint
	end,

	isValidNorthEast = function (x, z, map)
		if (x >= map.width - 1 or z == 0) then return false end --Top row or Right row
	    --Check diagonals
	    if (not map:isWalkable(x + 1, z - 1) and (map:isWalkable(x + 1, z) and map:isWalkable(x, z - 1))) then
	       
	        return true
	    else
	        return false
	    end
	end,

	 isValidNorthWest = function (x, z, map)

		if (x ==0 or z == 0) then return false end --Top row or Left row
	    --Check diagonals
	    if (not map:isWalkable(x - 1, z - 1) and (map:isWalkable(x - 1, z) and map:isWalkable(x, z - 1))) then
	        return true
	    else
	        return false
	    end
	end,

	isValidSouthEast = function (x, z, map)
		if (x >= map.width - 1 or z >= map.height - 1) then return false end --Top row or Right row
	    --Check diagonals
	    if (not map:isWalkable(x + 1, z + 1) and (map:isWalkable(x + 1, z) and map:isWalkable(x, z + 1))) then
	        return true
	    else
	        return false
	    end
	end,

	isValidSouthWest = function (x, z, map)
		if (x == 0 or z >= map.height - 1) then return false end --Top row or Right row
	    --Check diagonals
	    if (not map:isWalkable(x - 1, z + 1) and (map:isWalkable(x - 1, z) and map:isWalkable(x, z + 1))) then
	        return true
	    else
	        return false
	    end
	end


}