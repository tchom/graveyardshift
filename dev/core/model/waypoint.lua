return{
	new = function (x, z)
		
		local waypoint = {
			x = x or 0,
			z = z or 0,
			dirty = false,
			connections = {}
		}

		function waypoint:addConnection(waypoint)
			if not self:isConnected(waypoint) then
				table.insert(self.connections, waypoint)
			end
		end

		function waypoint:removeConnection(waypoint)
			local connection = 0

			for i=1,#self.connections do
				if(self.connections[i]:equals(waypoint))then connection = i end
			end

			if(connection > 0) then
				table.remove(self.connections, connection)
			end
		end

		function waypoint:isConnected(waypoint)
			local connected = false

			for i=1,#self.connections do
				if self.connections[i]:equalsValue(waypoint) then
					connected = true
				end
			end

			return connected
		end


		function waypoint:equals(waypoint)
			return (self.x == waypoint.x and self.z == waypoint.z and self.dirty == waypoint.dirty)
		end

		function waypoint:equalsValue(waypoint)
			return (self.x == waypoint.x and self.z == waypoint.z)
		end

		function waypoint:clone()
			local orig_type = type(self)
		    local copy
		    if orig_type == 'table' then
		        copy = {}
		        for orig_key, orig_value in pairs(self) do
		            copy[orig_key] = orig_value
		        end
		    else -- number, string, boolean, etc
		        copy = self
		    end
		    return copy
		end

		
		return waypoint
	end
}