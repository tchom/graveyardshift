local ModelModule = require "core/model/modelmodule"

local calendarModule = ModelModule.new("calendar")

calendarModule.currentTime = 16 * GameSettings.SECONDS_PER_GAMEHOUR
calendarModule.year = 1
calendarModule.day = 1
calendarModule.hour = 1
calendarModule.minute = 0
calendarModule.parsedTime = ""
calendarModule.weekdayIndex = 0
calendarModule.seasonIndex = 0




function calendarModule:update (dt)
	self.currentTime = self.currentTime + dt
	self.hour = math.floor( self.currentTime / GameSettings.SECONDS_PER_GAMEHOUR )
	self.minute = math.floor( self.currentTime%GameSettings.SECONDS_PER_GAMEHOUR)/GameSettings.SECONDS_PER_GAMEHOUR*GameSettings.GAMEMINUTES_PER_GAMEHOUR

	-- New day rocks
	if(self.currentTime >= (GameSettings.SECONDS_PER_GAMEHOUR * GameSettings.GAMEHOUR_PER_GAMEDAY)) then
		self.currentTime = 0
		if self.weekdayIndex < #GameSettings.WEEKDAYS then
			self.weekdayIndex = self.weekdayIndex+1
		else
			self.weekdayIndex = 1
		end

		self.day = self.day + 1
		self.hour = 0

	end

	self:parseTime() 
end

function calendarModule:parseTime ()
	local hourString = self.hour
	if self.hour >  12 then
		hourString = self.hour - 12
	elseif self.hour == 0  then
		hourString = 12
	end

	local minuteString = math.floor( self.minute)
	if #tostring(minuteString) <=1 then
		minuteString = "0"..minuteString
	end

	local meridiem = "am"
	if self.hour >=12 then
		meridiem = "pm"
	end

	self.parsedTime = hourString..":"..minuteString.." "..meridiem

end

function calendarModule:getTimeColour ()
	local nextHour = self.hour+2
	if(nextHour > 24)then nextHour = 1 end

	local hourPerc = self.minute / GameSettings.GAMEMINUTES_PER_GAMEHOUR

	local currentColour = GameSettings.TIME_TINTS[self.hour+1]
	local nextColour = GameSettings.TIME_TINTS[nextHour]

	local r = self:lerp(currentColour[1], nextColour[1], hourPerc)
	local g = self:lerp(currentColour[2], nextColour[2], hourPerc)
	local b = self:lerp(currentColour[3], nextColour[3], hourPerc)


	return {r, g, b}
end

function calendarModule:lerp(a,b,t) 
	return (1-t)*a + t*b 
end


return calendarModule