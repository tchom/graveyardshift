local MapChunk = require( "core/model/mapchunk" )

return{
	new = function(_id, _width, _height)

		local map = {
			id = _id,
			width = _width,
			height = _height,
			tiledMap = require("assets/maps/".._id),
			chunks = {},
			tileset = {}
		}

		if (_width%GameSettings.CHUNK_SIZE ~= 0) or (_height%GameSettings.CHUNK_SIZE ~= 0) then
			error("Map size must be a multiple of "..GameSettings.CHUNK_SIZE, 1)
		end

		map.chunksAcross = _width/GameSettings.CHUNK_SIZE
		map.chunksDown = _height/GameSettings.CHUNK_SIZE

		for j = 0, map.chunksDown-1 do
			for i = 0, map.chunksAcross-1 do
				--map.chunks[i + (j*map.width) + 1] = MapChunk.new(i, j, map.chunksAcross, map.chunksDown)
				table.insert(map.chunks, MapChunk.new(i, j, GameSettings.CHUNK_SIZE, GameSettings.CHUNK_SIZE))
			end
		end

		function map:addTileData(x, z, td)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)
			local tileX = x % GameSettings.CHUNK_SIZE
			local tileZ = z % GameSettings.CHUNK_SIZE

			local chunk = self:getChunk(chunkX, chunkZ)
			chunk:addTileData(tileX, tileZ, td)

			--self.tiles[(x + z*self.width+ 1)] = td
		end

		function map:addWaypoint(waypoint)
			local chunkX = math.floor(waypoint.x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(waypoint.z / GameSettings.CHUNK_SIZE)

			local chunk = self:getChunk(chunkX, chunkZ)
			chunk:addWaypoint(waypoint)

		end


		function map:clearAllWaypoints()
			for i = 1, #self.chunks do
				self.chunks[i].waypoints={}
			end
		end

		function map:getChunk(chunkX, chunkZ)
			local chunk = self.chunks[chunkX + (chunkZ*self.chunksAcross) + 1]
			return chunk
		end

		function map:getChunkByTile(tileX, tileZ)
			local chunkX = math.floor(tileX / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(tileZ / GameSettings.CHUNK_SIZE)

			local chunk = self.chunks[chunkX + (chunkZ*self.chunksAcross) + 1]
			return chunk
		end

		function map:getTileData(x, z)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)
			local tileX = x % GameSettings.CHUNK_SIZE
			local tileZ = z % GameSettings.CHUNK_SIZE


			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown) then
				local chunk = self:getChunk(chunkX, chunkZ)
				return chunk:getTileData(tileX, tileZ)
			else
				return nil
			end
		end

		function map:setTileset(tileset)
			self.tileset = tileset

		end

		function map:getTilesetTile(index)
			return self.tileset.tiles[index]
		end

		function map:getTilesetTileByType(type)
			return self.tileset:getTileIndexByType(type)
		end

		function map:getTilesetTileAtPosition(x, z, layerName)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)
			local tileX = x % GameSettings.CHUNK_SIZE
			local tileZ = z % GameSettings.CHUNK_SIZE

			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown and
				tileX >= 0 and tileX < GameSettings.CHUNK_SIZE and chunkZ >= 0 and tileZ < GameSettings.CHUNK_SIZE) then
				local chunk = self:getChunk(chunkX, chunkZ)
				local tileData = chunk:getTileData(tileX, tileZ)
				return self.tileset.tiles[tileData[layerName]]
				
			end
			return nil
		end

		function map:isWalkable(x, z)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)
			local tileX = x % GameSettings.CHUNK_SIZE
			local tileZ = z % GameSettings.CHUNK_SIZE

			if (chunkX >= 0 and chunkX < self.chunksAcross and chunkZ >= 0 and chunkZ < self.chunksDown) then
				local tileData = self:getChunk(chunkX, chunkZ):getTileData(tileX, tileZ)
				return tileData:isWalkable()
			else

				return false
			end
		end

		function map:tileIsLegal(x, z)
			return (x >= 0 and x < self.width and z >= 0 and z < self.height)
		end


		function map:chunkIsLegal(chunkX, chunkZ)
			return (chunkX >= 0 and chunkX < self.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown) 
		end

		function map:isResource(x, z, layerName)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)
			local tileX = x % GameSettings.CHUNK_SIZE
			local tileZ = z % GameSettings.CHUNK_SIZE

			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown and
				tileX >= 0 and tileX < GameSettings.CHUNK_SIZE and chunkZ >= 0 and tileZ < GameSettings.CHUNK_SIZE) then
				local chunk = self:getChunk(chunkX, chunkZ)
				local tileData = chunk:getTileData(tileX, tileZ)
				local tile = self.tileset.tiles[tileData[layerName]]

				if tile == nil then
					return false
				else
					return (tile.resourcesOnHit ~= nil)
						or (tile.resourcesOnDestroy ~= nil)
				end
				
			end
			return false 
		end

		function map:addGrave(grave, x, z)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)

			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown)  then

				local chunk = self:getChunk(chunkX, chunkZ)
				chunk:addGrave(grave)

			end

		end

		function map:getGrave(x, z)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)

			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown)  then

				local chunk = self:getChunk(chunkX, chunkZ)
				return chunk:getGrave(x, z)
			else
				return nil
			end

		end

		function map:hasGrave(x, z)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)

			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown)  then

				local chunk = self:getChunk(chunkX, chunkZ)
				return chunk:hasGrave(x, z)
			else
				return false
			end
		end

		function map:addPortal(x, z, portal)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)


			local chunk = self:getChunk(chunkX, chunkZ)
			chunk:addPortal(portal)

			--self.tiles[(x + z*self.width+ 1)] = td
		end

		function map:getPortal(x, z)
			local chunkX = math.floor(x / GameSettings.CHUNK_SIZE)
			local chunkZ = math.floor(z / GameSettings.CHUNK_SIZE)
			


			if (chunkX >= 0 and chunkX < map.chunksAcross and chunkZ >= 0 and chunkZ < map.chunksDown) then
				local chunk = self:getChunk(chunkX, chunkZ)
				return chunk:getPortal(x, z)
			else
				return nil
			end
		end

		function map:getPortalByTarget(target, out)
			local portal = nil

			for i = 1, #self.chunks do
				local chunk = self.chunks[i]
				local potentialPortal = chunk:getPortalByTarget(target, out)

				if (not portal) then
					portal = potentialPortal
				elseif(potentialPortal and potentialPortal.target ==  target and potentialPortal.out == out) then
					-- Override a default
					portal = potentialPortal
				end
			end


			return portal
		end

		return map
	end
}