local ModelModule = require "core/model/modelmodule"

local playerModule = ModelModule.new("player")

playerModule.fullEnergy = 30
playerModule.currentEnergy = playerModule.fullEnergy
playerModule.currentMoney = 100
playerModule.nextDebt = 1000
playerModule.selectedItem = nil


function playerModule:update (dt)
	
end

function playerModule:selectItem(newItem)
	if newItem ~= nil  then
		if (not self.selectedItem or newItem.id ~= self.selectedItem.id) then
			self.selectedItem = newItem
		end
	end
	
end

function playerModule:addMoney(newMoney)
	self.currentMoney = self.currentMoney + newMoney
end

function playerModule:restoreEnergy()
	self.currentEnergy = self.fullEnergy
end


return playerModule