return{
	new = function(_ground, _props, _groundWalkable, _propsWalkable, _groundHitspoints, _propsHitpoints, _groundHeightOffset, _propsHeightOffset)
		local td = {
			ground = _ground,
			groundWalkable = _groundWalkable,
			groundHitpoints = _groundHitspoints,
			groundHeightOffset = _groundHeightOffset or 0,

			props = _props,
			propsWalkable = _propsWalkable,
			propsHitpoints = _propsHitpoints,
			propsHeightOffset = _propsHeightOffset or 0,

		}

		function td:isWalkable()
			return  (self.groundWalkable and self.propsWalkable)
		end

		return td
	end
}