return {
	new = function(_chance)
		local hr = {
			chance = _chance,
			resources = {}
		}

		function hr:addResource(_name, _value)
			local resource = {
				name = _name,
				value = _value
			}

			table.insert(self.resources, resource)
		end




		return hr
	end
}