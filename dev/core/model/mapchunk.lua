return{
	new = function(_posX, _posZ, _width, _height)

		local mapChunk = {
			posX = _posX,
			posZ = _posZ,
			width = _width,
			height = _height,
			tiles={},
			graves={},
			portals={},
			waypoints={}
		}


		function mapChunk:addWaypoint(waypoint)
			
			table.insert(self.waypoints, waypoint)
		end

		function mapChunk:clearAllWaypoints()
			for i = 1, #self.waypoints do
				local waypoint = self.waypoints[i]

				for j = 1, #waypoint.connections do
					waypoint.connections[j]:removeConnection(waypoint)
				end
			end
			self.waypoints = {}
		end

		function mapChunk:addTileData(x, z, td)
			self.tiles[(x + z*self.width+ 1)] = td
		end

		function mapChunk:getTileData(x, z)
			if (x >= 0 and x < self.width and z >= 0 and z < self.height) then
				return self.tiles[(x + z*self.width+ 1)]
			else
				return nil
			end
		end

		function mapChunk:addGrave(grave)
			table.insert(self.graves, grave)
		end

		function mapChunk:getGrave(_posX, _posZ)

			for i = 1, #self.graves do
				if self.graves[i]:hit(_posX, _posZ) then
					return self.graves[i]

				end
			end

			return nil
		end

		function mapChunk:hasGrave(_posX, _posZ)
			local hit = false

			for i = 1, #self.graves do
				if self.graves[i]:hit(_posX, _posZ) then
					hit = true
				end
			end

			return hit
		end

		function mapChunk:addPortal(portal)
			table.insert(self.portals, portal)
		end

		function mapChunk:getPortal(_posX, _posZ)
			for i = 1, #self.portals do
				if(self.portals[i]:hit(_posX, _posZ)) then
					return self.portals[i]
				end
			end

			return nil
		end

		function mapChunk:getPortalByTarget(target, out)
			local default = nil

			for i = 1, #self.portals do
				if(self.portals[i].default) then
					print("Default found")
					default = self.portals[i] 
				end
				if(self.portals[i].target == target and self.portals[i].out == out) then
					return self.portals[i]
				end
			end

			return default
		end


		return mapChunk
	end
}