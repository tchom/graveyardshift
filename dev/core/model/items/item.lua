local Vec2 = require "core/geom/vec2"

return {
	new = function(_id, _name, _action, _category, _cursorMode, _texture, _group, _target, _properties)
		local item = {
			id = _id,
			name = _name,
			action = _action,
			category = _category,
			cursorMode = _cursorMode, 
			icon = _texture or "",
			group = _group or {Vec2.new(0, 0)},
			target = _target or nil,
			properties = _properties or nil

		}

		return item
	end
}