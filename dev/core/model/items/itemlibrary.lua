local ModelModule = require "core/model/modelmodule"

local itemLibraryModule = ModelModule.new("itemlibrary")

itemLibraryModule.items = {}

function itemLibraryModule:update (dt)
	
end

function itemLibraryModule:addItem (item)
	table.insert(self.items, item)
end

function itemLibraryModule:getItem (itemId)
	local item = nil

	for i = 1, #self.items do
		if self.items[i].id == itemId then
			item = self.items[i]
		end
	end

	return item
end

function itemLibraryModule:getItemsInCategory (categoryName)
	local category = {}

	for i = 1, #self.items do
		if self.items[i].category == categoryName then
			table.insert(category, self.items[i])
		end
	end

	return category
end



return itemLibraryModule