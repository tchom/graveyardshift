return {
	new = function(_posX, _posZ, _out, _target, _trigger_adjacent, _default)
		local portal = {
			posX = _posX, 
			posZ = _posZ, 
			out = _out, 
			target = _target, 
			trigger_adjacent = _trigger_adjacent,
			default = _default

		}

		function portal:hit(_hitX, _hitZ)
			return _hitX == self.posX and _hitZ == self.posZ
		end

		return portal
	end
}