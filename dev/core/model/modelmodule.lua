return{
	new = function(id)
		assert(id)

		local model_module = {__id = id}

		function model_module:update(dt)

		end

		function model_module:serialise()

		end

		function model_module:deserialise()

		end

		return model_module

	end
}