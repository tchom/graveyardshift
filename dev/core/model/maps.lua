local ModelModule = require "core/model/modelmodule"
local Map = require "core/model/map"
local mapsModule = ModelModule.new("maps")

mapsModule.previousMap = ""
mapsModule.currentMap = ""

mapsModule.maps = {}

function mapsModule:addMap ( _id, _width, _height)
	self.maps[_id] = Map.new(_id, _width, _height)
	return self.maps[_id]
end

function mapsModule:getMap ( _id )
	assert(self.maps[_id])

	return self.maps[_id]
end

function mapsModule:hasMap ( _id )
	return (self.maps[_id] ~= nil)
end



function mapsModule:getCurrent ()
	return self:getCurrentMap ()
end

function mapsModule:getCurrentMap ()
	assert(self.maps[self.currentMap])
	return self.maps[self.currentMap]
end

return mapsModule