--local PriorityQueue = require "lib/PriorityQueue"
local binaryheap = require 'lib/binaryheap'

return{
	calculatePath = function(self, start, goal)
		local frontier = binaryheap.minUnique()

		frontier:insert(0, start)

		cameFrom = {}
		costSoFar = {}

		cameFrom[start] = start
    	costSoFar[start] = 0

    	

		while not frontier:empty() do
			local priority, current = frontier:pop()

			--frontier:remove(current)
			

			if current:equals(goal) then
				break
			end

			for i = 1, #current.connections do
				
				local next = current.connections[i]
				local distValue = self.getDistSquared(next.x, next.z, current.x, current.z)
				local newCost = costSoFar[current] + distValue

				if not costSoFar[next] or newCost < costSoFar[next] then
					costSoFar[next] = newCost
					local priority = newCost + self.heuristic(next, goal)
					frontier:insert(priority, next)
					cameFrom[next] = current

				end
			end
		end

		local current = goal
		local pathIsBroken = false
		local path = {}

		table.insert(path, current)

		while not current:equals(start) and not pathIsBroken do
			if cameFrom[current] or not current:equals(goal) then
				current = cameFrom[current]
            	table.insert(path, current)
			else
				 pathIsBroken = true
			end
		end

		if pathIsBroken then
			return nil
		else
			
			return table.reverse(path)
		end
		
	end,

	getDistSquared = function(x1, y1, x2, y2)
		return ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
	end,

	heuristic = function(a, b)
		return math.abs( b.x - a.x ) + math.abs(b.z - a.z)
	end

}