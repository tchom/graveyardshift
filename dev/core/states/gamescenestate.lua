require "core/cameras/camera"
require "core/utils" -- math.round

-- states/gamescenestate.lua


local GeomUtils = require "core/geom/geom_helper"
local Vec2 = require "core/geom/vec2"

local GameScene = Game:addState('GameScene')



function GameScene:enteredState()
	love.graphics.clear()
	love.graphics.setBackgroundColor(0, 0, 0)

	self.World = require "core/entities/world"
	_G.Game.Command:execute("initialise_game_scene", self.World)

	local previousMap = _G.Game.Model:getModule("maps").previousMap
	local currentMap = _G.Game.Model:getModule("maps"):getCurrentMap()
	self.calendar = _G.Game.Model:getModule("calendar")


	local startPortal = currentMap:getPortalByTarget(previousMap, false)

	self.player = self.World:getAllWith({"player"})[1]
	local body = self.player:get("iso_transform")
	body.position.x = startPortal.posX
	body.position.z = startPortal.posZ

	self.MainUI = require "core/ui/gamesceneui".new()


end

function GameScene:exitedState()
	print("Destroyed the world")
	--cleanup scene
	self.MainUI:destroy()
	self.World:destroy()
end



function GameScene:update(dt)
	self.calendar:update(dt)
	_G.Game.Mouse.update(dt)
	
	self.MainUI:update(dt)
	local body = self.player:get("iso_transform")
	local bodyScreenPoint = GeomUtils.isoToScreen(body.position)

	local targetCameraX = bodyScreenPoint.x*GameSettings.TILE_SIZE - love.graphics.getWidth( ) * 0.5
	local targetCameraY = bodyScreenPoint.y*GameSettings.TILE_SIZE - love.graphics.getHeight( ) * 0.5

	camera.position.x = (targetCameraX + camera.position.x*9)/10
	camera.position.y = (targetCameraY + camera.position.y*9)/10

	GameSettings.CAMERA_POSITION_X = camera.position.x
	GameSettings.CAMERA_POSITION_Y = camera.position.y

	self.World:update(dt)

		if love.keyboard.isDown "escape" then
	        love.event.quit()
	    end

	    _G.Game.Model:update(dt)
end


function GameScene:draw()

	local body = self.player:get("iso_transform")
	local bodyScreenPoint = GeomUtils.isoToScreen(body.position)
	local shader = _G.Game.AssetsManager:getShader("light-shader")

	shader:send("light_positions", {bodyScreenPoint.x*GameSettings.TILE_SIZE, bodyScreenPoint.y*GameSettings.TILE_SIZE}, {-500, 100},{500, 500},{0, 500})
    
    shader:send("light_size", 200)
   -- shader:send("ambient_light", {25/255, 33/255, 41/255})
   	local colour = self.calendar:getTimeColour()
   	shader:send("ambient_light", {colour[1]/255, colour[2]/255, colour[3]/255})
   --	shader:send("ambient_light", {222/255, 216/255, 201/255})
    shader:send("number_of_lights", 2)
    shader:send("camera", {GameSettings.CAMERA_POSITION_X, GameSettings.CAMERA_POSITION_Y})
    love.graphics.setShader(shader)

	camera:set()
	self.World:draw()
	camera:unset()

	love.graphics.setShader()

	love.graphics.setColor(0, 0, 0)
	love.graphics.print("FPS " .. love.timer.getFPS(), 2, 2)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print("FPS " .. love.timer.getFPS(), 0, 0)
	local player = _G.Game.Model:getModule("player")
	love.graphics.setColor(0, 0, 0)
	love.graphics.print("Selected item: "..player.selectedItem.name, 2, 22)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print("Selected item: "..player.selectedItem.name, 0, 20)

	self.MainUI:draw()

end

function GameScene:mousepressed(x, y, button, istouch)
	self.MainUI:mousepressed(x, y, button, istouch)
end

function GameScene:mousereleased(x, y, button, istouch)
	self.MainUI:mousereleased(x, y, button, istouch)
end


function GameScene:keypressed(key, code)
  if key == 'p' then
    self:pushState('Pause')
  end
end
