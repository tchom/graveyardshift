-- states/gameloadstate.lua

--[[_G.Game = {
    GameLoop = require "modules/gameloop",
    Renderer = require "modules/renderer",
    World    = require "modules/entity_world",
    GSM      = require "modules/game_scene_manager",
    Assets   = require "modules/assets",
    Physics  = require "modules/physics_engine",
    Camera   = Reloader:require( "modules/camera"),

    Canvas   = love.graphics.newCanvas(1920, 1080),
}]]--




local GameLoad = Game:addState('GameLoad')

function GameLoad:enteredState()
  

  self:createModel()
  self:registerCommands()

  _G.Game.Command:execute("initialise_game")

end

function GameLoad:createModel()
  _G.Game.Model:addModule("maps", require("core/model/maps"))
  _G.Game.Model:addModule("player", require("core/model/player/player"))
  _G.Game.Model:addModule("itemlibrary", require("core/model/items/itemlibrary"))
  _G.Game.Model:addModule("calendar", require("core/model/calendar/calendar"))
end

function GameLoad:registerCommands()
  local mapsModel = _G.Game.Model:getModule("maps")

  -- ASSETS COMMANDS
  _G.Game.Command:registerCommand(require("core/commands/assets/createtilesetcommand")
                                    :initialise(_G.Game.AssetsManager))

  _G.Game.Command:registerCommand(require("core/commands/assets/createtilesetfromtiledcommand")
                                    :initialise(_G.Game.AssetsManager))

  _G.Game.Command:registerCommand(require("core/commands/assets/createtextureatlascommand")
                                    :initialise(_G.Game.AssetsManager))

  _G.Game.Command:registerCommand(require("core/commands/assets/createshadercommand")
                                    :initialise(_G.Game.AssetsManager))

  _G.Game.Command:registerCommand(require("core/commands/assets/loadassetscommand")
                                    :initialise(_G.Game.AssetsManager))

  -- GAME COMMANDS
   _G.Game.Command:registerCommand(require("core/commands/game/initialisegamecommand")
                                    :initialise())

  -- SCENE COMMANDS
   _G.Game.Command:registerCommand(require("core/commands/gamescene/initialisegamescenecommand"))


   _G.Game.Command:registerCommand(require("core/commands/map/loadmapcommand")
                                    :initialise(mapsModel))

   _G.Game.Command:registerCommand(require("core/commands/map/createmapcommand")
                                    :initialise(mapsModel))

   _G.Game.Command:registerCommand(require("core/commands/map/createmapwaypointscommand")
                                    :initialise())

   _G.Game.Command:registerCommand(require("core/commands/map/clearwaypointcommand")
                                    :initialise())

   _G.Game.Command:registerCommand(require("core/commands/map/recalculatewaypointsattilecommand"):initialise())

   -- ITEM COMMANDS
   _G.Game.Command:registerCommand(require("core/commands/items/initialiseitemlibrarycommand"))

   _G.Game.Command:registerCommand(require("core/commands/items/setactiveitemcommand"))
   
   _G.Game.Command:registerCommand(require("core/commands/items/performtoolactioncommand")
                                       :initialise())

      -- TOOLS COMMANDS
      _G.Game.Command:registerCommand(require("core/commands/items/tools/digactioncommand"))
      _G.Game.Command:registerCommand(require("core/commands/items/tools/chopactioncommand"))
      _G.Game.Command:registerCommand(require("core/commands/items/tools/mineactioncommand"))
      _G.Game.Command:registerCommand(require("core/commands/items/tools/interactactioncommand"))
      _G.Game.Command:registerCommand(require("core/commands/items/tools/buildgraveactioncommand"))

   -- UI COMMANDS   
   _G.Game.Command:registerCommand(require("core/commands/ui/createuimousehandlercommand"))
   _G.Game.Command:registerCommand(require("core/commands/ui/senduisignalcommand"))


   -- STATE COMMANDS
   _G.Game.Command:registerCommand(require("core/commands/state/changestatecommand")
                                    :initialise(_G.Game.GameState))
end

function GameLoad:update(dt)
 
end

function GameLoad:draw()
  -- Draw your menu stuff (buttons, etc.) here
  love.graphics.setBackgroundColor(0, 0, 0)
  love.graphics.print("Game Load")
end
