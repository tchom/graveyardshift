-- states/menustate.lua

local Menu = Game:addState('Menu')

function Menu:enteredState()
  self.titleImage = love.graphics.newImage("assets/screens/menu/title.png")
  self.ySpeed = 2
  self.yRange = 20
  self.time = 0
end

function Menu:update(dt)
  self.time = self.time + dt
  -- You should switch to another state here,
  -- Usually when a button is pressed.
  -- Either with gotoState() or pushState()
end

function Menu:draw()
  -- Draw your menu stuff (buttons, etc.) here
  local offsetY = 150 + (math.sin( self.time * self.ySpeed)) * self.yRange
  love.graphics.setBackgroundColor(0, 0, 0)
  love.graphics.draw(self.titleImage, love.graphics.getWidth( )/2, offsetY, 0, 1, 1, self.titleImage:getWidth() / 2, 0)
  love.graphics.setColor(255,255,255,255)
  love.graphics.print("Press space")
end

function Menu:keypressed(key, code)
	if key == 'space' then
    	self:gotoState('GameLoad')
	end

	if love.keyboard.isDown "escape" then
        love.event.quit()
	end
end