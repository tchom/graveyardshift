return {
	new = function(_name, _type, _walkable, _hitpoints, _resourcesOnHit, _resourcesOnDestroy, _heightOffset)
		local t = {
			name = _name,
			type = _type,
			walkable = _walkable,
			hitpoints = _hitpoints,
			resourcesOnHit = _resourcesOnHit,
			resourcesOnDestroy = _resourcesOnDestroy,
			heightOffset = _heightOffset or 0
		}


		return t
	end
}