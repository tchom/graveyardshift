return {
	new = function()

		local assetsManager = {
			tilesets = {},
			shaders = {},
			textureAtlas = {},
			fonts = {}
		}

		function assetsManager:hasTileset(name)
			return (self.tilesets[name] ~= nil)
		end

		function assetsManager:addTileset(name, tileset)
			self.tilesets[name] = tileset
		end

		function assetsManager:getTileset(name)
			assert( self.tilesets[name] )
			return self.tilesets[name]
		end

		function assetsManager:hasTextureAtlas(name)
			return (self.textureAtlas[name] ~= nil)
		end

		function assetsManager:addTextureAtlas(name, textureAtlas)
			self.textureAtlas[name] = textureAtlas
		end

		function assetsManager:getTextureAtlas(name)
			assert( self.textureAtlas[name] )
			return self.textureAtlas[name]
		end

		function assetsManager:hasShader(name)
			return (self.shaders[name] ~= nil)
		end

		function assetsManager:addShader(name, shader)
			self.shaders[name] = shader
		end

		function assetsManager:getShader(name)
			assert( self.shaders[name] )
			return self.shaders[name]
		end

		function assetsManager:setFont(name, size)
			path = "assets/font/"..name..".ttf"

		    if not self.fonts[path..size] then
		        self.fonts[path..size] = love.graphics.newFont(path, size)
		    end
		    
		    local font = self.fonts[path..size]

		    love.graphics.setFont(font)
		end


		return assetsManager
	end
}