return{
	new = function(_id)

		local tileset = {
			id = _id,
			tiles = {},
			frames = {}
		}

		function tileset:getTile(i)
			assert(self.tiles[i])
			return self.tiles[i]
		end

		function tileset:getTileWalkable(i)
			assert(self.tiles[i])
			return self.tiles[i].walkable
		end

		function tileset:getTileIndexByType(type)
			for key, value in pairs(self.tiles) do
				if value.type == type then return key end
			end

			return 0
		end

		return tileset
	end
}