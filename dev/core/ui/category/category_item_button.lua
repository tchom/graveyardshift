return {
	new = function (icon, command, commandParams)
		local CategoryItemButton = require 'core/ui/ui_component'.new()

		CategoryItemButton.x = 0
		CategoryItemButton.y = 0

		CategoryItemButton.width = 80
		CategoryItemButton.height = 80

		CategoryItemButton.icon = icon
		CategoryItemButton.command = command or ""
		CategoryItemButton.commandParams = commandParams or {}

		function CategoryItemButton:init()

			self.selected = false
			self.clickedState = false


			self.atlas = _G.Game.AssetsManager:getTextureAtlas("game_scene_ui")
			self.texture = self.atlas.texture
			self.buttonBackQuad = self.atlas.quads["slot_empty"]

			self.iconQuad = self.atlas.quads[self.icon]
			self.iconSize = self.atlas.size[self.icon]

			CategoryItemButton:_init()

		end

		function CategoryItemButton:setIcon(icon)
			
		end

		function CategoryItemButton:inBounds()
			local mouseX = love.mouse.getX()
			local mouseY = love.mouse.getY()
			return mouseX > self.x and mouseX < self.x + self.width and  mouseY > self.y and mouseY < self.y + self.height  
		end

		function CategoryItemButton:update(dt)
			self:_update(dt)
		end

		function CategoryItemButton:draw()
			love.graphics.draw(self.texture, self.buttonBackQuad, self.x, self.y)

			love.graphics.draw(self.texture, self.iconQuad, self.x + (self.width-self.iconSize.width)*0.5, self.y+ (self.height-self.iconSize.height)*0.5)


		  	self:_draw()
		end

		function CategoryItemButton:select()
			self.selected = true
			self.buttonBackQuad = self.atlas.quads["slot_selected"]
		end

		function CategoryItemButton:deselect()
			self.selected = false
			self.buttonBackQuad = self.atlas.quads["slot_empty"]
		end

		function CategoryItemButton:mousereleased(x, y, button, istouch)
			if self.clickedState == false then 
				if self:inBounds() then
					_G.Game.Mouse.cancel()
					self.clickedState = true -- see
				end
			else
				self.clickedState = false
			end

			return self.clickedState
		end

		CategoryItemButton:init()

		return CategoryItemButton
	end
}
