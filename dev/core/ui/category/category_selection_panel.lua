local CloseButton = require("core/ui/close_button")
local CategoryItemButton = require("core/ui/category/category_item_button")

return{
	new = function(id)
		local CategorySelectionPanelUI = require 'core/ui/ui_component'.new(id)

		function CategorySelectionPanelUI:init()
			self.x = 130
			self.y = 40
			
			local atlas = _G.Game.AssetsManager:getTextureAtlas("game_scene_ui")
			self.backingTexture = atlas.texture
			self.backingQuad = atlas.quads["inventory_backing"]
			self.backingSize = atlas.size["inventory_backing"]
			self.width = self.backingSize.width
			self.height = self.backingSize.height
			
			self.category = ""
			self.categoryDescription = ""
			self.buttons = {}

			self.closeButton = CloseButton.new("send_ui_signal", {"category_selection_panel", "close"})
			self.closeButton.x = 482+self.x
			self.closeButton.y = 27+self.y
			self:addChildComponent(self.closeButton)

			self.visible = false

		end

		function CategorySelectionPanelUI:update(dt)
			if not self.visible then return end

			self:_update(dt)

			if(_G.Game.Mouse.isDown) then
				if self:inBounds() then
					_G.Game.Mouse.cancel()
				end
			end
		end

		function CategorySelectionPanelUI:draw()
			if not self.visible then return end
			love.graphics.draw(self.backingTexture, self.backingQuad, self.x, self.y)
			love.graphics.setColor(238, 234, 217)
			_G.Game.AssetsManager:setFont("knightsquest", 40)
			love.graphics.printf(self.categoryDescription, self.x+self.backingSize.width*0.5-100, self.y+40, 200, "center")
			_G.Game.AssetsManager:setFont("verdana", 12)
			self:_draw()
		end

		function CategorySelectionPanelUI:mousereleased(x, y, button, istouch)
			if self:inBounds() then
				self.closeButton:mousereleased(x, y, button, istouch)



				local selectedIndex = 0
				for i = 1, #self.buttons do
					if self.buttons[i]:mousereleased(x, y, button, istouch) then
						selectedIndex = i
					end
				end

				if(selectedIndex > 0) then
					for i = 1, #self.buttons do
						if i == selectedIndex then
							self.buttons[i]:select()
							_G.Game.Command:execute(self.buttons[i].command, unpack(self.buttons[i].commandParams))
							_G.Game.Command:execute("send_ui_signal", "category_selection_panel", "close")
							_G.Game.Mouse.cancel()
						else
							self.buttons[i]:deselect()
						end
					end
				end
				return true
			end
			
			return false
		end

		function CategorySelectionPanelUI:inBounds()
			local mouseX = love.mouse.getX()
			local mouseY = love.mouse.getY()
			return mouseX > self.x and mouseX < self.x + self.width and  mouseY > self.y and mouseY < self.y + self.height  
		end

		function CategorySelectionPanelUI:show(params)
			self.buttons = {}
			self.category = params[1]
			self.categoryDescription = params[2]

			local categoryItems = _G.Game.Model:getModule("itemlibrary"):getItemsInCategory(self.category)

			local offsetX = 48 + self.x
			local offsetY = 148 + self.y

			for i = 1, #categoryItems do
				local catgeoryItem = categoryItems[i]

				local button = CategoryItemButton.new(catgeoryItem.icon, "set_active_item", {catgeoryItem.id})

				button.x = offsetX + ((i-1)%5)*(button.width + 10)
				button.y = offsetY + math.floor( (i-1)/5 )*(button.height + 10)

				table.insert(self.buttons, button)

				self:addChildComponent(button)
			end


			self.visible = true
		end

		return CategorySelectionPanelUI
	end
}