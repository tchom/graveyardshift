return {
	new = function (command, commandParams)
		local CloseButton = require 'core/ui/ui_component'.new()

		CloseButton.command = command or ""
		CloseButton.commandParams = commandParams or {}

		function CloseButton:init()
			self.x = self.x or 0
			self.y = self.y or 0

			self.atlas = _G.Game.AssetsManager:getTextureAtlas("game_scene_ui")
			self.texture = self.atlas.texture
			self.upQuad = self.atlas.quads["buttons/closebutton_up"]
			self.overQuad = self.atlas.quads["buttons/closebutton_over"]
			self.downQuad = self.atlas.quads["buttons/closebutton_down"]
			self.buttonSize = self.atlas.size["buttons/closebutton_up"]
			self.activeQuad = self.upQuad

			self.width = self.buttonSize.width
			self.height = self.buttonSize.height

			CloseButton:_init()

		end

		function CloseButton:setIcon(icon)
			
		end

		function CloseButton:inBounds()
			local mouseX = love.mouse.getX()
			local mouseY = love.mouse.getY()
			return mouseX > self.x and mouseX < self.x + self.width and  mouseY > self.y and mouseY < self.y + self.height  
		end

		function CloseButton:update(dt)
			if self:inBounds() then
				self.activeQuad = self.overQuad
			else
				self.activeQuad = self.upQuad
			end

			self:_update(dt)
		end

		function CloseButton:draw()
			love.graphics.draw(self.texture, self.activeQuad, self.x, self.y)

		  	self:_draw()
		end


		function CloseButton:mousereleased(x, y, button, istouch)
			if self:inBounds() then
				_G.Game.Mouse.cancel()
				_G.Game.Command:execute(self.command, unpack(self.commandParams))
				self.clickedState = true -- see
			end
			
		end

		CloseButton:init()

		return CloseButton
	end
}
