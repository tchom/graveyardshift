return{
	new = function(id)

		local UIComponent = {
			visible = true,
			children = {},
			id = id or ""
		}

		function UIComponent:init()
			
		end

		function UIComponent:_init()
			for i = 1, #self.children do
				self.children[i]:init()
				self.children[i]:_init()
			end
		end

		function UIComponent:addChildComponent(child)
			table.insert(self.children, child)
		end

		function UIComponent:update(dt)
			self:_update(dt)
		end

		function UIComponent:_update(dt)
			self:updateChildren(dt)
		end

		function UIComponent:updateChildren(dt)
			for i = 1, #self.children do
				self.children[i]:update(dt)
			end
		end

		function UIComponent:draw()
			self:_draw()
		end

		function UIComponent:_draw()
			self:drawChildren()
		end

		function UIComponent:drawChildren()
			for i = 1, #self.children do
				self.children[i]:draw()
			end
		end

		function UIComponent:show(params)
			self.visible = true
		end

		function UIComponent:hide(params)
			self.visible = false
		end

		function UIComponent:destroy()
			
		end

		function UIComponent:mousepressed(x, y, button, istouch)
			for i = 1, #self.children do
				self.children[i]:mousepressed(x, y, button, istouch)
			end
		end

		function UIComponent:mousereleased(x, y, button, istouch)
			for i = 1, #self.children do
				self.children[i]:mousereleased(x, y, button, istouch)
			end
		end

		return UIComponent
	end
}