return{
	new = function()
		local CalendarPanelUI = require 'core/ui/ui_component'.new()

		function CalendarPanelUI:init()
			self.calendar = _G.Game.Model:getModule("calendar")

			local atlas = _G.Game.AssetsManager:getTextureAtlas("game_scene_ui")
			self.backingTexture = atlas.texture
			self.backingQuad = atlas.quads["time_circle"]
			self.backingSize = atlas.size["time_circle"]

			self.position = {
				x = love.graphics.getWidth() - self.backingSize.width,
				y = -40
			}

		end

		function CalendarPanelUI:update(dt)
			self:_update(dt)
		end

		function CalendarPanelUI:draw()
			love.graphics.draw(self.backingTexture, self.backingQuad, self.position.x, self.position.y)
			love.graphics.setColor(255, 255, 255)
			_G.Game.AssetsManager:setFont("knightsquest", 28)
			love.graphics.printf(self.calendar.parsedTime, self.position.x-15, 20, 200, "center")
			_G.Game.AssetsManager:setFont("verdana", 12)
			self:_draw()
		end


		return CalendarPanelUI
	end
}