return {
	new = function()
		local GameSceneUI = require 'core/ui/ui_component'.new()

		function GameSceneUI:init()
			_G.Game.UISignal:add(GameSceneUI.handleSignalMessage, GameSceneUI)
			self:addChildComponent(require("core/ui/calendar_panel_ui").new())
			self:addChildComponent(require("core/ui/toolbar_panel_ui").new())
			self:addChildComponent(require("core/ui/category/category_selection_panel").new("category_selection_panel"))
			GameSceneUI:_init()


		end

		function GameSceneUI:update(dt)
			self:_update(dt)
		end

		function GameSceneUI:draw()
		  -- Draw your menu stuff (buttons, etc.) here
		  self:_draw()
		end

		function GameSceneUI:handleSignalMessage(target, message, params)
			if(message == "open") then
			 	for i = 1, #self.children do

			 		
				 	if(self.children[i].id == target) then
				 		self.children[i]:show(params)
				 	end

				 end

			elseif(message == "close") then
			 	for i = 1, #self.children do

				 	if(self.children[i].id == target) then
				 		self.children[i]:hide(params)
				 	end
				 end
			end
		end

		function GameSceneUI:destroy()
			_G.Game.UISignal:remove(self.handleSignalMessage, self)
		end


		GameSceneUI:init()

		return GameSceneUI
	end
}

