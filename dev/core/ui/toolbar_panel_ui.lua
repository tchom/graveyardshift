local ToolButton = require("core/ui/tool_button")
return {
	new = function()
		
		local ItemPanelUI = require 'core/ui/ui_component'.new()


		function ItemPanelUI:init()
			self.buttons = {}
			self.rotation = 0
			self.scale = 1
			self.position = {
				x = 0,
				y = 80
			}

			local atlas = _G.Game.AssetsManager:getTextureAtlas("game_scene_ui")
			self.backingTexture = atlas.texture
			self.backingQuad = atlas.quads["toolbar_backing"]

			local toolbarLayout = require 'layout/ui/toolbar_panel_layout'
			for i = 1, #toolbarLayout.buttons do
				local toolbarItem = toolbarLayout.buttons[i]

				local button = ToolButton.new(toolbarItem.icon, toolbarItem.command, toolbarItem.commandParams)

				button.x = 10
				button.y =  80+ (button.height + 10)*(i-1)

				table.insert(self.buttons, button)

				self:addChildComponent(button)

			end
		end

		function ItemPanelUI:update(dt)
			if(_G.Game.Mouse.isDown) then
				for i = 1, #self.buttons do
					if self.buttons[i]:inBounds() then
						_G.Game.Mouse.cancel()
					end
				end
			end

			self:_update(dt)
		end

		function ItemPanelUI:draw()

			--[love.graphics.draw(self.backingTexture, self.backingQuad, 0, 0)]] 
			self:_draw()

		end

		function ItemPanelUI:mousepressed(x, y, button, istouch)
			local selectedIndex = 0
			for i = 1, #self.buttons do
				if self.buttons[i]:mousereleased(x, y, button, istouch) then
					selectedIndex = i
				end
			end

			if(selectedIndex > 0) then
				for i = 1, #self.buttons do
					if i == selectedIndex then
						self.buttons[i]:select()
						_G.Game.Command:execute(self.buttons[i].command, unpack(self.buttons[i].commandParams))
						_G.Game.Mouse.cancel()
					else
						self.buttons[i]:deselect()
					end
				end
			end
		end

		return ItemPanelUI
	end
}