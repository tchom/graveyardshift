return {
	new = function (icon, command, commandParams)
		local ToolButton = require 'core/ui/ui_component'.new()

		ToolButton.width = 80
		ToolButton.height = 80

		ToolButton.icon = icon
		ToolButton.command = command or ""
		ToolButton.commandParams = commandParams or {}

		function ToolButton:init()
			self.x = self.x or 0
			self.y = self.y or 0

			self.selected = false
			self.clickedState = false


			self.atlas = _G.Game.AssetsManager:getTextureAtlas("game_scene_ui")
			self.texture = self.atlas.texture
			self.buttonBackQuad = self.atlas.quads["slot_empty"]

			self.iconQuad = self.atlas.quads[self.icon]
			self.iconSize = self.atlas.size[self.icon]

			ToolButton:_init()

		end

		function ToolButton:setIcon(icon)
			
		end

		function ToolButton:inBounds()
			local mouseX = love.mouse.getX()
			local mouseY = love.mouse.getY()
			return mouseX > self.x and mouseX < self.x + self.width and  mouseY > self.y and mouseY < self.y + self.height  
		end

		function ToolButton:update(dt)
			self:_update(dt)
		end

		function ToolButton:draw()
			love.graphics.draw(self.texture, self.buttonBackQuad, self.x, self.y)

			love.graphics.draw(self.texture, self.iconQuad, self.x + (self.width-self.iconSize.width)*0.5, self.y+ (self.height-self.iconSize.height)*0.5)


		  	self:_draw()
		end

		function ToolButton:select()
			self.selected = true
			self.buttonBackQuad = self.atlas.quads["slot_selected"]
		end

		function ToolButton:deselect()
			self.selected = false
			self.buttonBackQuad = self.atlas.quads["slot_empty"]
		end

		function ToolButton:mousereleased(x, y, button, istouch)
			if self.clickedState == false then 
				if self:inBounds() then
					_G.Game.Mouse.cancel()
					self.clickedState = true -- see
				end
			else
				self.clickedState = false
			end

			return self.clickedState
		end

		ToolButton:init()

		return ToolButton
	end
}
