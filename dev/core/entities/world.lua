local Entity = require "core/entities/entity"

local World = {
	entities = {},
	systems = {}

}

function World:destroy()
	self.systems = {}
	for i=#self.entities,1,-1 do
		self.entities[i]:destroy();
	end

	self.entities = {}
end

function World:register(system)
	table.insert(self.systems, system)

	return system
end



function World:getAllWith(requires)
	local matches = {}

	for i=1,#self.entities do
		local ent = self.entities[i]
		local matched = true

		for j=1,#requires do
			if ent:get(requires[j]) == nil then
				matched = false
			end
		end

		if matched then table.insert(matches, ent) end

	end

	
	return matches
end

function World:assemble(components)
	local ent = self:create()

	for i,v in ipairs(components) do
		assert(type(v) == "table", "World:Assemble requires a table of tables")
		assert(#v > 0)
		local fn = v[1]
		assert(type(fn) == "function")

		if #v == 1 then
			ent:add(fn())
		else
			local args = {}
			for i=2,#v do
				table.insert(args, v[i])
			end

			ent:add(fn(unpack(args)))
		end

	end

	return ent
end

function World:create()
	local entity = Entity.new()
	table.insert(self.entities, entity)
	return entity
end

function World:update(dt)
	for i=#self.entities,1,-1 do
		local entity = self.entities[i]
		if entity.remove then
			-- TODO: replace this with a standard loop
			for i, system in ipairs(self.systems) do
				if system:match(entity) then
					system:destroy(entity)
				end
			end

			table.remove(self.entities, i)
		else

			-- TODO: replace this with a standard loop
			for i, system in ipairs(self.systems) do
				if system:match(entity) then
					if entity.loaded == false then
						system:load(entity)
					end

					system:update(dt, entity)
				end
			end	

			entity.loaded = true		
		end
	end
end

function World:sort(tbl)
	table.sort(tbl, function(a, b) return a:get("iso_transform"):getDepth() < b:get("iso_transform"):getDepth() end)

	return tbl
end

function World:draw()
	local isoEntities = self:getAllWith({"iso_transform"})
	local sorted =  self:sort(isoEntities)

	--[[-- TODO: implement renderer
	for i=1,#self.entities do
		local entity = self.entities[i]
		-- TODO: replace this with a standard loop
		for i, system in ipairs(self.systems) do
			if system:match(entity) then
				system:draw(entity)
			end
		end
	end]]--
	

	for i=1,#sorted do
		local entity = sorted[i]
		-- TODO: replace this with a standard loop
		for i, system in ipairs(self.systems) do
			if system:match(entity) then
				system:draw(entity)
			end
		end
	end
end

return World