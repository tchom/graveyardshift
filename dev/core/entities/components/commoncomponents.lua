local Component = require "core/entities/components/component"
local Vec2 = require "core/geom/vec2"
local Vec3 = require "core/geom/vec3"

-- AI modules
local BehaviourTree = require "core/entities/components/ai/behaviourtree"
local Blackboard = require "core/entities/components/ai/blackboard"


AtlasImporter = require "lib/AtlasImporter"
TexMate = require "lib/TexMate"

return {
	
	 new_body = function(x, y)
		local body = Component.new("body")
		body.x = x or 0
		body.y = y or 0

		return body
	end,

	new_iso_transform = function(x, y, z, size, isHeightObject, priority)
		local iso_transform = Component.new("iso_transform")
		iso_transform.position = Vec3.new(x or 0, y or 0, z or 0)
		
		iso_transform.size = size or GameSettings.TILE_SIZE
		iso_transform.isHeightObject = isHeightObject
		iso_transform.height = 0
		iso_transform.priority = priority or 0

		function iso_transform:getDepth()

			return (self.position.x + (self.position.z*1.001)) * 0.866 - self.position.y * 0.707 + self.priority 
		end

		return iso_transform
	end,


	new_sprite = function(start_anim, myAtlas, frames, offsetX, offsetY)
		local spriteComponent = Component.new("sprite_component")
		local offsetX = offsetX or 0
		local offsetY = offsetY or 0

		spriteComponent.offset = Vec2.new()
		
		spriteComponent.sprite = TexMate(myAtlas, frames, start_anim,nil,nil,offsetX,offsetY)

		return spriteComponent
	end,

	new_iso_tile_component = function(camera, x, z, layerName, map )
		local isoTile = Component.new("iso_tile")
		isoTile.camera = camera
		isoTile.offset = {}
		isoTile.offset.x = x
		isoTile.offset.z = z
		isoTile.layerName = layerName
		isoTile.map = map

		return isoTile
	end,



	new_player_component = function(root)
		local player = Component.new("player")
		
		return player
	end,

	new_ai_component = function(root)
		local ai = Component.new("ai")
		ai._blackboard = Blackboard.new()
		ai._tree = BehaviourTree.new()
		ai._tree.root = root
		return ai
	end,

	

	functional = function(fn)
		local functional = Component.new("functional")
		functional.fn = fn
		return functional
	end

	
}