local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local portal = tick.blackboard:get("portal", tick.tree.id, nil)
	 		
	 		_G.Game.Command:execute("load_map", portal.target)

	 		return AI_UTILS.states.SUCCESS
	 		
	 	end

	 	return node
	 end
}

