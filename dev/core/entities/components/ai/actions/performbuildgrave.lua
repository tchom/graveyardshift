local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local GeomUtils = require "core/geom/geom_helper"

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)
	 

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local player = _G.Game.Model:getModule("player")
	 		local isoTarget = tick.blackboard:get("isoTarget", tick.tree.id, nil)
	 		local tileX =  math.round( isoTarget.x)
	 		local tileZ =  math.round( isoTarget.z)

			_G.Game.Command:execute("build_grave_action", tileX, tileZ)

	 		return AI_UTILS.states.SUCCESS
	 	end

	 	return node
	 end
}

