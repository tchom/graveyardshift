local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local Vec2 = require "core/geom/vec2"
local GeomUtils = require "core/geom/geom_helper"

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)

	 		local velocity = 3* tick.deltaTime

	 		local dx = self.target.x - self.body.position.x
			local dz = self.target.z - self.body.position.z
			local angle = math.atan2(dz, dx)

			self.body.position.x = self.body.position.x + velocity * math.cos(angle) 
			self.body.position.z = self.body.position.z + velocity * math.sin(angle)

	 		
	 		if((dx*dx + dz*dz) < (velocity*velocity)) then
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.RUNNING
	 		end
	 	end

	 	function node:open(tick)
	 		self.body = tick.target:get("iso_transform")
	 		local mouseX, mouseY = love.mouse.getPosition( )
      		local screenPos = Vec2.new((mouseX + GameSettings.CAMERA_POSITION_X)/GameSettings.TILE_SIZE, (mouseY - 32 + GameSettings.CAMERA_POSITION_Y)/GameSettings.TILE_SIZE)
			self.target = GeomUtils.screenToIso(screenPos)
	 	end

	 	return node
	 end
}

