local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local GeomUtils = require "core/geom/geom_helper"

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)
	 	node.currentTime = 0
	 	node.endTime = arg[1]

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		self.currentTime = self.currentTime + tick.deltaTime
	 		if(self.currentTime > self.endTime) then
	 			return AI_UTILS.states.SUCCESS
	 		end

	 		return AI_UTILS.states.RUNNING
	 		
	 	end

	 	function node:open(tick)
	 		self.currentTime = 0

	 	end

	 	return node
	 end
}

