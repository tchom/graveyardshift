local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local WaypointUtil = require "core/model/utils/waypointutils"
local Vec2 = require "core/geom/vec2"
local Vec3 = require "core/geom/vec3"
local GeomUtils = require "core/geom/geom_helper"
local Waypoint = require "core/model/waypoint"
local Astar = require "core/math/astar"



return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local path = tick.blackboard:get("move_path", tick.tree.id, nil)

	 		if path then
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.FAILURE
	 		end

	 		
	 	end

	 	function node:open(tick)
	 		local path = nil
	 		local isoTarget = tick.blackboard:get("isoTarget", tick.tree.id, nil)
	 		local tileX =  math.round( isoTarget.x)
	 		local tileZ =  math.round( isoTarget.z)

	 		local isoTransformComponent = tick.target:get("iso_transform")
	 		local bodyX = math.round( isoTransformComponent.position.x)
	 		local bodyZ = math.round( isoTransformComponent.position.z)

	 		if 	(tileX == bodyX-1 and tileZ == bodyZ ) 
	 			or(tileX == bodyX+1 and tileZ == bodyZ ) 
	 			or(tileX == bodyX and tileZ == bodyZ-1 ) 
	 			or(tileX == bodyX and tileZ == bodyZ+1 ) then 
	 			-- already adjacent
	 			tick.blackboard:set("move_path", {}, tick.tree.id, nil)
	 			return
	 		end

	 		local map = _G.Game.Model:getModule("maps"):getCurrentMap()


	 		local adjacentTiles = {Vec2.new(tileX, tileZ-1), Vec2.new(tileX, tileZ+1), Vec2.new(tileX-1, tileZ), Vec2.new(tileX+1, tileZ)}

	 		for i = 1, #adjacentTiles do
	 			local adjacentTile = adjacentTiles[i]

	 			if(map:isWalkable(adjacentTile.x, adjacentTile.y)) then
	 				local newPath = nil
	 				local linePath = GeomUtils.calculateBreshamLine(bodyX, bodyZ, adjacentTile.x, adjacentTile.y)
			 		local isStraightPath = GeomUtils.checkValidPath(linePath, map)

			 		if isStraightPath then
			 			local entryWaypoint = Waypoint.new(bodyX, bodyZ)
			 			local exitWaypoint =Waypoint.new(adjacentTile.x, adjacentTile.y)

			 			newPath = {}
			 			table.insert(newPath, Vec3.new(bodyX, 0, bodyZ))
			 			table.insert(newPath, Vec3.new(adjacentTile.x, 0, adjacentTile.y))
			 			
			 		else
			 			local entryWaypoint = WaypointUtil:createConnectorWaypoint(map, bodyX, bodyZ, true)
			 			local exitWaypoint = WaypointUtil:createConnectorWaypoint(map, adjacentTile.x, adjacentTile.y, true)

			 			newPath = Astar:calculatePath(entryWaypoint, exitWaypoint)

			 			_G.Game.Command:execute("clear_waypoint", entryWaypoint)
						_G.Game.Command:execute("clear_waypoint", exitWaypoint)
			 		end

			 		if (not path and newPath) or (newPath and #path > #newPath) then
			 			path = newPath
			 		end

	 			end
	 		end

	 		tick.blackboard:set("move_path", path, tick.tree.id, nil)

	 	end

	 	return node
	 end
}

