local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local WaypointUtil = require "core/model/utils/waypointutils"
local Vec3 = require "core/geom/vec3"
local GeomUtils = require "core/geom/geom_helper"
local Waypoint = require "core/model/waypoint"
local Astar = require "core/math/astar"



return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local path = tick.blackboard:get("move_path", tick.tree.id, nil)

	 		if path then
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			print("Path error")
	 			return AI_UTILS.states.FAILURE
	 		end
	 	end

	 	function node:open(tick)
	 		local path = nil
	 		local isoTarget = tick.blackboard:get("isoTarget", tick.tree.id, nil)
	 		local tileX =  math.round( isoTarget.x)
	 		local tileZ =  math.round( isoTarget.z)

	 		local isoTransformComponent = tick.target:get("iso_transform")
	 		local bodyX = math.round( isoTransformComponent.position.x)
	 		local bodyZ = math.round( isoTransformComponent.position.z)


	 		local map = _G.Game.Model:getModule("maps"):getCurrentMap()

	 		local linePath = GeomUtils.calculateBreshamLine(bodyX, bodyZ, tileX, tileZ)
	 		local isStraightPath = GeomUtils.checkValidPath(linePath, map)

	 		if isStraightPath then
	 			local entryWaypoint = Waypoint.new(bodyX, bodyZ)
	 			local exitWaypoint =Waypoint.new(tileX, tileZ)

	 			local path = {}
	 			table.insert(path, Vec3.new(bodyX, 0, bodyZ))
	 			table.insert(path, Vec3.new(tileX, 0, tileZ))

	 			tick.blackboard:set("move_path", path, tick.tree.id, nil)
				--tick.blackboard:set("entry_waypoint", entryWaypoint, tick.tree.id, nil)
				--tick.blackboard:set("exit_waypoint", exitWaypoint, tick.tree.id, nil)
	 		else
	 			local entryWaypoint = WaypointUtil:createConnectorWaypoint(map, bodyX, bodyZ, true)
	 			local exitWaypoint = WaypointUtil:createConnectorWaypoint(map, tileX, tileZ, true)

	 			local path = Astar:calculatePath(entryWaypoint, exitWaypoint)

	 			tick.blackboard:set("move_path", path, tick.tree.id, nil)

	 			_G.Game.Command:execute("clear_waypoint", entryWaypoint)
				_G.Game.Command:execute("clear_waypoint", exitWaypoint)
				--tick.blackboard:set("entry_waypoint", entryWaypoint, tick.tree.id, nil)
				--tick.blackboard:set("exit_waypoint", exitWaypoint, tick.tree.id, nil)
	 		end
	 	end

	 	return node
	 end
}

