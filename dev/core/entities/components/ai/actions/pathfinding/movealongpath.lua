local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local WaypointUtil = require "core/model/utils/waypointutils"
local GeomUtils = require "core/geom/geom_helper"
local Waypoint = require "core/model/waypoint"
local Astar = require "core/math/astar"



return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local path = tick.blackboard:get("move_path", tick.tree.id, nil)

	 		if not path then
	 			return AI_UTILS.states.FAILURE
	 		end

	 		if #path < 1 then
	 			return AI_UTILS.states.SUCCESS
	 		end

	 		local target = path[1]

	 		local velocity = 3* tick.deltaTime

	 		local dx = target.x - self.body.position.x
			local dz = target.z - self.body.position.z
			local angle = math.atan2(dz, dx)

			self.body.position.x = self.body.position.x + velocity * math.cos(angle) 
			self.body.position.z = self.body.position.z + velocity * math.sin(angle)


			if((dx*dx + dz*dz) < (velocity*velocity)) then
				table.remove(path, 1)

				if #path < 1 then
					path = nil
					-- clean up
					
					return AI_UTILS.states.SUCCESS
				end

				tick.blackboard:set("move_path", path, tick.tree.id, nil)
			end

			return AI_UTILS.states.RUNNING
	 		
	 	end

	 	function node:open(tick)
	 		self.body = tick.target:get("iso_transform")
	 	end

	 	return node
	 end
}

