local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick, dt)
	 		
	 		for i = 1, #self.children do
	 			local status = self.children[i]:_execute(tick)
	 		end

	 		return AI_UTILS.states.SUCCESS
	 	end

	 	return node
	 end
}

