local  Tick = require "core/entities/components/ai/tick"

return {
	 new = function()
	 	local tree = {
	 		id = generateUUID(), -- in UTILS file
	 		root = nil
	 	}


	 	function tree:tick(target, blackboard, dt)
	 		-- create tick
	 		local tick =  Tick.new()
	 		tick.target = target
	 		tick.blackboard = blackboard
	 		tick.deltaTime = dt
	 		tick.tree = self
	 		-- tick node
	 		self.root:_execute(tick)
	 		-- close last node if needed
	 		local lastOpenNodes = blackboard:get('openNodes', self.id);
	 		local currentOpenNodes = table.slice(tick.openNodes, 1);

	 		-- does not close if it is open in this tick
	 		local start = 1

	 		for i = 1, math.min(#lastOpenNodes, #currentOpenNodes) do
	 			start = i +1
	 			if(lastOpenNodes[i] ~= currentOpenNodes[i]) then
	 				break
	 			end
	 		end

	 		-- close the open nodes
	 		for i = #lastOpenNodes, start do
	 			if lastOpenNodes[i] then
	 				lastOpenNodes[i]:_close(tick)
	 			end
	 		end

	 		-- POPULATE BLACKBOARD 
		    blackboard:set('openNodes', currentOpenNodes, self.id);
		    blackboard:set('nodeCount', tick.nodeCount, self.id);


	 	end

	 	return tree

	 end

}