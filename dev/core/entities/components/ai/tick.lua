return {
	 new = function()
	 	local tick = {
	 		tree = nil,
	 		openNodes = {},
	 		nodeCount = 0,
	 		deltaTime = 0,
	 		debug = nil,
	 		target = nil,
	 		blackboard = nil
	 	}

	 	function tick:enterNode(node)
	 		self.nodeCount = self.nodeCount + 1
	 		table.insert(self.openNodes, node)
	 	end

	 	function tick:openNode(node)

	 	end

	 	function tick:tickNode(node)

	 	end

	 	function tick:closeNode(node)
			table.remove(self.openNodes)
	 	end

	 	function tick:exitNode(node)

	 	end


	 	return tick
	 end
}