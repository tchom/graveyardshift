local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:open(tick)
	 		tick.blackboard:set("runningChild", 1, tick.tree.id, self.id)
	 	end

	 	function node:tick(tick)
	 		local child = tick.blackboard:get("runningChild",  tick.tree.id, self.id)

	 		for i = child, #self.children do
	 			local status = self.children[i]:_execute(tick)

	 			if(status ~= AI_UTILS.states.FAILURE) then
	 				if(status == AI_UTILS.states.RUNNING) then
	 					tick.blackboard:set("runningChild", i, tick.tree.id, self.id)
	 				end
	 				return status
	 			end
	 		end
	 		return AI_UTILS.states.FAILURE
	 	end

	 	return node
	 end
}
