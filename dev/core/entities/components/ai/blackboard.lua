return {
	 new = function()
	 	
	 	local blackboard = {
	 		_baseMemory = {}, -- used to store global information
    		_treeMemory = {} -- used to store tree and node information
	 	}

	 	function blackboard:_getTreeMemory(treeScope)
	 		if not self._treeMemory[treeScope] then
	 			self._treeMemory[treeScope] = {
	 				nodeMemory = {},
	 				openNodes = {}
	 			}
	 		end

	 		return self._treeMemory[treeScope]
	 	end

	 	function blackboard:_getNodeMemory(treeMemory, nodeScope)
	 		local memory = treeMemory['nodeMemory']

	 		if not memory[nodeScope] then
	 			memory[nodeScope] = {}
	 		end

	 		return memory[nodeScope]
	 	end

	 	function blackboard:_getMemory(treeScope, nodeScope)
	 		local memory = self._baseMemory

	 		if treeScope then
	 			memory = self:_getTreeMemory(treeScope)


	 			if nodeScope then
	 				memory = self:_getNodeMemory(memory, nodeScope)
	 			end
	 		end

	 		return memory
	 	end


	 	function blackboard:set(key, value, treeScope, nodeScope)
	 		local memory = self:_getMemory(treeScope, nodeScope)
   			memory[key] = value
	 	end

	 	function blackboard:get(key, treeScope, nodeScope)
	 		local memory = self:_getMemory(treeScope, nodeScope)
   			return memory[key]
	 	end
	 

	 	return blackboard
	 end
}