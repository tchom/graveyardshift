local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local Vec2 = require "core/geom/vec2"
local GeomUtils = require "core/geom/geom_helper"

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local map = _G.Game.Model:getModule("maps"):getCurrent()

	 		local isoTarget = tick.blackboard:get("isoTarget", tick.tree.id, nil)
	 		local tileX =  math.round( isoTarget.x)
	 		local tileZ =  math.round( isoTarget.z)


	 		if(map:isResource(tileX, tileZ, "props")) then
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.FAILURE
	 		end
	 	end

	 	return node
	 end
}
