local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local player = _G.Game.Model:getModule("player")
	 		if(player.selectedItem.action == "interact") then
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.FAILURE
	 		end
	 	end

	 	return node
	 end
}

