local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local isoTarget = tick.blackboard:get("isoTarget", tick.tree.id, nil)
	 		local tileX =  math.round( isoTarget.x)
	 		local tileZ =  math.round( isoTarget.z)

	 		local map = _G.Game.Model:getModule("maps"):getCurrentMap()
	 		--print(map.id.." "..tileX.."x"..tileZ)
	 		--print(map:isWalkable(tileX, tileZ))
	 		if(map:isWalkable(tileX, tileZ)) then
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.FAILURE
	 		end
	 	end

	 	return node
	 end
}

