local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter
local Vec2 = require "core/geom/vec2"
local GeomUtils = require "core/geom/geom_helper"

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		
	 		
	 		if(_G.Game.Mouse.worldClick()) then
	 			local mouseX, mouseY = love.mouse.getPosition( )
      			local screenPos = Vec2.new((mouseX + GameSettings.CAMERA_POSITION_X)/GameSettings.TILE_SIZE, (mouseY - 32 + GameSettings.CAMERA_POSITION_Y)/GameSettings.TILE_SIZE)
				local isoTarget = GeomUtils.screenToIso(screenPos)

	 			tick.blackboard:set("screenX", mouseX, tick.tree.id, nil)
	 			tick.blackboard:set("screenY", mouseY, tick.tree.id, nil)
	 			tick.blackboard:set("isoTarget", isoTarget, tick.tree.id, nil)
	 			
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.FAILURE
	 		end
	 	end

	 	return node
	 end
}

