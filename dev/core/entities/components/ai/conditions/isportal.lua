local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function(...)
	 	local node = require("core/entities/components/ai/basenode"):new(children)
	 	local arg={...}
	 	arg = table.slice(arg, 2)

	 	node:initialize(arg)

	 	function node:tick(tick)
	 		local isoTarget = tick.blackboard:get("isoTarget", tick.tree.id, nil)
	 		local tileX =  math.round( isoTarget.x)
	 		local tileZ =  math.round( isoTarget.z)

	 		local map = _G.Game.Model:getModule("maps"):getCurrentMap()
	 		local portal = map:getPortal(tileX, tileZ)
	 		if(portal and portal.out) then
	 			local isoTarget = tick.blackboard:set("portal", portal, tick.tree.id, nil)
	 			return AI_UTILS.states.SUCCESS
	 		else
	 			return AI_UTILS.states.FAILURE
	 		end
	 	end

	 	return node
	 end
}

