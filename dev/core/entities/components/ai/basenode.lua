local AI_UTILS = require "core/entities/components/ai/aiutils" -- TODO: Put this somewhere smarter

return {
	 new = function()
	 	local node = {
	 		id = generateUUID(),
	 		children = {}
	 	}


	 	function node:initialize(children)
		 	if children then
		 		for i = 1, #children do
		 			table.insert(node.children, children[i])
		 		end
		 	end
	 	end

	 	-- wrapper functions
	 	function node:_execute(tick)
	 		--enter
	 		self:_enter(tick)

	 		--open
	 		if not tick.blackboard:get('isOpen', tick.tree.id, self.id) then
	 			self:_open(tick);
	 		end

	 		-- tick
	 		local status = self:_tick(tick)

	 		--close
	 		if status ~= AI_UTILS.states.RUNNING then
	 			self:_close(tick)
	 		end

	 		-- exit
	 		self:_exit(tick)

	 		return status
	 	end

	 	function node:_enter(tick)
	 		tick:enterNode(this)
    		self:enter(tick);
	 	end

	 	function node:_open(tick)
	 		tick:openNode(self);
		    tick.blackboard:set('isOpen', true, tick.tree.id, self.id);
		    self:open(tick);
	 	end

	 	function node:_tick(tick)
	 		tick:tickNode(self);
    		return self:tick(tick);
	 	end

	 	function node:_close(tick)
	 		tick:closeNode(self);
		    tick.blackboard:set('isOpen', false, tick.tree.id, self.id);
		    self:close(tick);
	 	end

	 	function node:_exit(tick)
	 		tick:exitNode(self);
    		self:exit(tick);
	 	end

	 	-- Override these to create nodes
	 	function node:execute(tick)end
	 	function node:enter(tick)end
	 	function node:open(tick)end
	 	function node:tick(tick)end
	 	function node:close(tick)end
	 	function node:exit(tick)end
	 	
	 	return node

	 end

}