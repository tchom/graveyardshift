local System = require "core/entities/systems/system"
local GeomUtils = require "core/geom/geom_helper"


return {
	new_renderer_system = function ()
		local renderer = System.new({"iso_transform", "sprite_component"})

		function renderer:load(entity)
			
		end


		function renderer:draw(entity)
			local iso_transform = entity:get("iso_transform")
			local spriteComponent = entity:get("sprite_component")
			local screenPos = GeomUtils.isoToScreen(iso_transform.position)

			--if not spriteComponent.sprite:getCurrentFrame() then return end
			--print( iso_transform.position.x.."x"..iso_transform.position.z )

			local sWidth = spriteComponent.sprite:getCurrentFrame().width
			local sHeight = spriteComponent.sprite:getCurrentFrame().height

			if iso_transform.isHeightObject then
				local sHeight = spriteComponent.sprite:getCurrentFrame().height
				iso_transform.height = sHeight -  (GameSettings.REAL_TILE_HEIGHT)
			else
				iso_transform.height = 0
			end

			spriteComponent.sprite.x = screenPos.x*GameSettings.TILE_SIZE 
			spriteComponent.sprite.y = screenPos.y*GameSettings.TILE_SIZE -  iso_transform.height
			spriteComponent.sprite:draw()
		end

		return renderer;
	end,

	new_iso_tile_system = function ()
		local system = System.new({"iso_transform", "iso_tile", "sprite_component"})

		function system:load(entity)
			
		end

		function system:update(dt, entity)
			local iso_transform = entity:get("iso_transform")
			local iso_tile = entity:get("iso_tile")
			local sprite = entity:get("sprite_component")

			local cameraIso = iso_tile.camera:getIsoPosition();

			iso_transform.position.x = iso_tile.offset.x + cameraIso.x - (cameraIso.x % 1);
			iso_transform.position.z = iso_tile.offset.z + cameraIso.z -  (cameraIso.z % 1);

			local currentTileX = math.round( iso_transform.position.x )
			local currentTileZ = math.round( iso_transform.position.z )

			local tileData = iso_tile.map:getTileData(currentTileX, currentTileZ)
			if not(tileData == nil) then
				iso_transform.position.y = 0.2*tileData[iso_tile.layerName.."HeightOffset"];

				local tile = iso_tile.map:getTilesetTile( tileData[iso_tile.layerName])
				if not(tile == nil or tile == '') then
					
				
					sprite.sprite:changeAnim(tile.name)
					
					sprite.sprite.active = true
				else
					sprite.sprite.active = false
				end
			else
				sprite.sprite.active = false
			end

		end

		return system;
	end,


	new_ai_system = function ()
		local system = System.new({"ai"})


		function system:update(dt, entity)
			local ai_component = entity:get("ai")
			ai_component._tree:tick(entity, ai_component._blackboard, dt)

		end

		return system;
	end,

	


	new_functional_system = function()
		local system = System.new({"functional"})

		function system:load(entity)
			--print("Found one")
		end

		function system:update(dt, entity)
			local fn = entity:get("functional").fn
			fn(entity, dt)

		end

		return system;
	end
}
