local GeomUtils = require "core/geom/geom_helper"
local Vec3 = require "core/geom/vec3"
local Vec2 = require "core/geom/vec2"

camera = {}
camera.position = Vec2.new(0, 0)
camera.scaleX = 1
camera.scaleY = 1
camera.rotation = 0
camera.isoPosition = Vec3.new(0, 0, 0)

function camera:set()
  love.graphics.push()
  love.graphics.rotate(-self.rotation)
  love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
  love.graphics.translate(-self.position.x, -self.position.y)

  self.isoPosition = GeomUtils.screenToIso(Vec2.new(self.position.x /64, self.position.y / 64))
end

function camera:unset()
  love.graphics.pop()
end

function camera:move(dx, dy)
  self.position.x = self.position.x + (dx or 0)
  self.position.y = self.position.y + (dy or 0)
end

function camera:rotate(dr)
  self.rotation = self.rotation + dr
end

function camera:scale(sx, sy)
  sx = sx or 1
  self.scaleX = self.scaleX * sx
  self.scaleY = self.scaleY * (sy or sx)
end

function camera:setPosition(x, y)
  self.position.x = x or self.position.x
  self.position.y = y or self.position.y
end

function camera:setScale(sx, sy)
  self.scaleX = sx or self.scaleX
  self.scaleY = sy or self.scaleY
end

function camera:getIsoPosition()
  
  return self.isoPosition
end