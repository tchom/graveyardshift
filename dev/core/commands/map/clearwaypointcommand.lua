local command = require "core/commands/command"

local ClearWaypointCommand = command.new("clear_waypoint")

function ClearWaypointCommand:initialise(...)
	local arg={...}
	
	return self
end

function ClearWaypointCommand:execute(...)
	local arg={...}
	local waypoint = arg[1]

	for i = 1, #waypoint.connections do
		local connectionWaypoint = waypoint.connections[i]

		connectionWaypoint:removeConnection(waypoint)
	end

end


return ClearWaypointCommand