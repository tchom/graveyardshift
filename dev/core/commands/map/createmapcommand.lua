local command = require "core/commands/command"
local Portal = require "core/model/map/portal"
local TileData = require "core/model/tiles/tiledata"

local CreateMapCommand = command.new("create_map")

function CreateMapCommand:initialise(...)
	local arg={...}
	self.maps = arg[1]
	
	return self
end

function CreateMapCommand:execute(...)
	local arg={...}
	local newMapName = arg[1]

	local tiledMap = require("assets/maps/"..newMapName)
	-- create map tileset
	_G.Game.Command:execute("create_tileset_from_tiled", "world_tiles", tiledMap, "../../../design/maps/cemetery/")

	-- store layers for easy access
	local layers = {}
	for i = 1, #tiledMap.layers do
		local layer = tiledMap.layers[i]
		print(layer.name)
		layers[layer.name] = layer
	end
	
	local newMap = self.maps:addMap(newMapName, tiledMap.width, tiledMap.height)
	local tileset = _G.Game.AssetsManager:getTileset("world_tiles")
	newMap:setTileset(tileset)

	--create tiledata
	for y=0,tiledMap.height-1 do
		for x=0,tiledMap.width-1 do
			local ground = layers["ground_tiles"].data[x+y*tiledMap.width + 1]
			local groundHitpoints = 0
			local groundWalkable = false
			local props = layers["props_tiles"].data[x+y*tiledMap.width + 1]
			local propsWalkable = true
			local groundHeightOffset = 0
			local propsHeightOffset = 0
			local propsHitpoints = 0

			if(ground ~= 0) then 
				groundHitpoints = tileset.tiles[ground].hitpoints
				groundWalkable = tileset.tiles[ground].walkable
				groundHeightOffset = tileset.tiles[ground].heightOffset

			end
			-- props are walkable by default
			if(props ~= 0) then 
				propsHeightOffset = tileset.tiles[props].heightOffset
				propsWalkable = tileset.tiles[props].walkable
				propsHitpoints = tileset.tiles[ground].hitpoints

			end

			
			

			local tileData = TileData.new(ground, props, groundWalkable, propsWalkable, groundHitpoints, propsHitpoints, groundHeightOffset, propsHeightOffset)
			newMap:addTileData(x, y, tileData)
		
		end
	end

	-- CREATE PORTALS
	for i = 1, #layers["portals"].objects do
		local posX = math.floor( layers["portals"].objects[i].x / GameSettings.TILE_SIZE)
		local posZ = math.floor( layers["portals"].objects[i].y / GameSettings.TILE_SIZE)
		local out = layers["portals"].objects[i].properties.out or true
		local trigger_adjacent = layers["portals"].objects[i].properties.trigger_adjacent or false
		local default = layers["portals"].objects[i].properties.default or false
		local target = layers["portals"].objects[i].properties.target
		local portal = Portal.new(posX, posZ, out, target, trigger_adjacent, default)

		newMap:addPortal(posX, posZ, portal)
	end

	_G.Game.Command:execute("create_map_waypoints", newMap)


end


return CreateMapCommand