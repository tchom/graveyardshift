local command = require "core/commands/command"
local WaypointUtil = require "core/model/utils/waypointutils"
local Waypoint = require "core/model/waypoint"
local GeomUtils = require "core/geom/geom_helper"

local RecalculateWaypointsAtTileCommand = command.new("recalculate_waypoints_at_tile")

function RecalculateWaypointsAtTileCommand:initialise(...)
	local arg={...}
	
	return self
end

function RecalculateWaypointsAtTileCommand:execute(...)
	local arg={...}
	local tileX = arg[1]
	local tileZ = arg[2]
	local map = _G.Game.Model:getModule("maps"):getCurrent()
	local chunk = map:getChunkByTile(tileX, tileZ)
	chunk:clearAllWaypoints()

	for z = 1, chunk.height do
		for x = 1, chunk.width do
			local xOffset = (chunk.posX * chunk.width) + x
			local zOffset = (chunk.posZ * chunk.height) + z


			if map:isWalkable(xOffset, zOffset) and WaypointUtil:isWaypoint(xOffset, zOffset, map) then
				local waypoint = Waypoint.new(xOffset, zOffset )
				map:addWaypoint(waypoint)
			end
		end
	end

	for j = chunk.posZ-1, chunk.posZ+1 do
		for i = chunk.posX-1, chunk.posX+1 do
			self:createConnections(map, chunk, i, j)
		end
	end

	for i = 1, #chunk.waypoints do
		local waypoint = chunk.waypoints[i]
		local connectionsToRemove = {}

		for j = 1, #waypoint.connections do
			local connectedWaypoint =  waypoint.connections[j]
			local path = GeomUtils.calculateBreshamLine(waypoint.x, waypoint.z, connectedWaypoint.x, connectedWaypoint.z)
			local legalPath = GeomUtils.checkValidPath(path, map)
			if not legalPath then table.insert(connectionsToRemove, connectedWaypoint) end
		end

		for k = 1, #connectionsToRemove do
			local invalidWaypoint = connectionsToRemove[k]
			waypoint:removeConnection(invalidWaypoint)
			invalidWaypoint:removeConnection(waypoint)

		end
	end

end





function RecalculateWaypointsAtTileCommand:createConnections(map, mapChunk, x, y)
	
	if (x < 0 or x > map.chunksAcross-1 or y < 0 or y > map.chunksDown- 1) then 
		return 
	end

	
	local connectedChunk = map:getChunk(x, y)

	for i = 1, #mapChunk.waypoints do
		local waypoint = mapChunk.waypoints[i]

		for j = 1, #connectedChunk.waypoints do
			local connectedWaypoint = connectedChunk.waypoints[j]

			if not connectedWaypoint:equals(waypoint) then
				
				waypoint:addConnection(connectedWaypoint)
				connectedWaypoint:addConnection(waypoint)
			end
		end
	end

end


return RecalculateWaypointsAtTileCommand