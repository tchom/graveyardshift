local command = require "core/commands/command"

local LoadMapCommand = command.new("load_map")

function LoadMapCommand:initialise(...)
	local arg={...}
	
	self.maps = arg[1]	
	return self
end

function LoadMapCommand:execute(...)
	local arg={...}
	local newMap = arg[1]

	self.maps.previousMap = self.maps.currentMap
	self.maps.currentMap = newMap

	if not self.maps:hasMap(newMap) then
		print("CREATE NEW MAP:: "..newMap)
		_G.Game.Command:execute("create_map", newMap)
	end

	_G.Game.Command:execute("change_state", "GameScene")

end


return LoadMapCommand