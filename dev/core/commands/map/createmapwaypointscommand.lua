local command = require "core/commands/command"
local Waypoint = require "core/model/waypoint"
local WaypointUtil = require "core/model/utils/waypointutils"
local GeomUtils = require "core/geom/geom_helper"

local CreateMapWaypointsCommand = command.new("create_map_waypoints")

function CreateMapWaypointsCommand:initialise(...)
	local arg={...}
	
	
	return self
end

function CreateMapWaypointsCommand:execute(...)
	local arg={...}
	local targetMap = arg[1]

	targetMap:clearAllWaypoints();
	self:generateWaypoints(targetMap)


end

function CreateMapWaypointsCommand:generateWaypoints(map)
	for z = 0, map.height do
		for x = 0, map.width do
			if(map:isWalkable(x,z)) and WaypointUtil:isWaypoint(x, z, map) then
				local waypoint = Waypoint.new(x, z)
				map:addWaypoint(waypoint)

			end
		end
	end


	for j = 0, map.chunksDown-1 do
		for i = 0, map.chunksAcross-1 do
			self:getChunkWaypointNeighbours(map, i, j)
		end
	end

end

function CreateMapWaypointsCommand:getChunkWaypointNeighbours(map, x, y)
	local currentMapChunk = map:getChunk(x, y)

	
	for j = y,  y+1 do

		for i = x, x+1 do

			self:createConnections(map, currentMapChunk, i, j)
		end
	end



	for i = 1, #currentMapChunk.waypoints do
		local waypoint = currentMapChunk.waypoints[i]
		local connectionsToRemove = {}

		for j = 1, #waypoint.connections do
			local connectedWaypoint =  waypoint.connections[j]
			local path = GeomUtils.calculateBreshamLine(waypoint.x, waypoint.z, connectedWaypoint.x, connectedWaypoint.z)
			local legalPath = GeomUtils.checkValidPath(path, map)
			if not legalPath then table.insert(connectionsToRemove, connectedWaypoint) end
		end

		for k = 1, #connectionsToRemove do
			local invalidWaypoint = connectionsToRemove[k]
			waypoint:removeConnection(invalidWaypoint)
			invalidWaypoint:removeConnection(waypoint)

		end
	end
end


function CreateMapWaypointsCommand:createConnections(map, mapChunk, x, y)
	
	if (x < 0 or x > map.chunksAcross-1 or y < 0 or y > map.chunksDown- 1) then 
		return 
	end

	
	local connectedChunk = map:getChunk(x, y)

	for i = 1, #mapChunk.waypoints do
		local waypoint = mapChunk.waypoints[i]

		for j = 1, #connectedChunk.waypoints do
			local connectedWaypoint = connectedChunk.waypoints[j]

			if not connectedWaypoint:equals(waypoint) then
				
				waypoint:addConnection(connectedWaypoint)
				connectedWaypoint:addConnection(waypoint)
			end
		end
	end

end



return CreateMapWaypointsCommand