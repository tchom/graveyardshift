local command = require "core/commands/command"

local CreateUIMouseHandlerCommand = command.new("create_ui_signal_mouse_handler")

function CreateUIMouseHandlerCommand:initialise(...)
	local arg={...}
	
	return self
end

function CreateUIMouseHandlerCommand:execute(...)
	local arg={...}
	-- Create UI Signal
	_G.Game.UISignal = require("lib/signal").new()

	_G.Game.Mouse = {}
	_G.Game.Mouse.wasClicked = false
	_G.Game.Mouse.isDown = false
	_G.Game.Mouse.prevDown = false
	_G.Game.Mouse.canClick = true
	_G.Game.Mouse.cancelled = false
	_G.Game.Mouse.update = function(dt)

		
		_G.Game.Mouse.wasClicked = false
		local mouseDown = love.mouse.isDown(1)


		_G.Game.Mouse.isDown = mouseDown
		

		if mouseDown ~=  _G.Game.Mouse.prevDown then
			if(mouseDown) then
				_G.Game.Mouse.cancelled = false
			else
				_G.Game.Mouse.wasClicked = true
			end
		end


		_G.Game.Mouse.prevDown = mouseDown

	end

	_G.Game.Mouse.cancel = function()
		_G.Game.Mouse.cancelled = true
	end

	_G.Game.Mouse.worldClick = function()
		return _G.Game.Mouse.wasClicked  and (not _G.Game.Mouse.cancelled)
	end



end


return CreateUIMouseHandlerCommand