local command = require "core/commands/command"

local SendUISignalCommand = command.new("send_ui_signal")

function SendUISignalCommand:initialise(...)
	local arg={...}
	
	return self
end

function SendUISignalCommand:execute(...)
	_G.Game.UISignal:dispatch(...)

end


return SendUISignalCommand