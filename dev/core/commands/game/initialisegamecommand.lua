local command = require "core/commands/command"

local InitialiseGameCommand = command.new("initialise_game")

function InitialiseGameCommand:initialise(...)
	local arg={...}
	
	return self
end

function InitialiseGameCommand:execute(...)
	local arg={...}

	


	-- MACRO COMMAND FOR LAUNCHING GAME RESOURCES
	_G.Game.Command:execute("load_assets")
	_G.Game.Command:execute("create_ui_signal_mouse_handler")
	_G.Game.Command:execute("initialise_item_library")
	_G.Game.Command:execute("set_active_item", "interact")
	_G.Game.Command:execute("load_map", "cemetery")
end


return InitialiseGameCommand