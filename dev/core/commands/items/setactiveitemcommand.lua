local command = require "core/commands/command"


local SetActiveItemCommand = command.new("set_active_item")

function SetActiveItemCommand:initialise(...)
	local arg={...}
	
	return self
end

function SetActiveItemCommand:execute(...)
	local arg={...}

	--print (arg[1])
	local player = _G.Game.Model:getModule("player")
	local itemLibrary = _G.Game.Model:getModule("itemlibrary")

	local selectedItem = itemLibrary:getItem(arg[1])

	player:selectItem(selectedItem)
	--player:selectItem(selectedItem)

end


return SetActiveItemCommand