local command = require "core/commands/command"
local TileData = require "core/model/tiles/tiledata"

local PerformToolActionCommand = command.new("perform_tool_action")

function PerformToolActionCommand:initialise(...)
	local arg={...}
	self.maps = arg[1]
	
	return self
end

function PerformToolActionCommand:execute(...)
	local arg={...}
	local tileX = arg[1]
	local tileZ = arg[2]
	local tool = arg[3]
	
	if tool == "interact" then _G.Game.Command:execute("interact_action", tileX, tileZ)
	elseif tool == "dig" then _G.Game.Command:execute("dig_action", tileX, tileZ)
	elseif tool == "chop" then _G.Game.Command:execute("chop_action", tileX, tileZ)
	elseif tool == "mine" then _G.Game.Command:execute("mine_action", tileX, tileZ)
	end

end


return PerformToolActionCommand