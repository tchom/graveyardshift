local command = require "core/commands/command"
local Item = require "core/model/items/item"


local InitialiseItemLibraryCommand = command.new("initialise_item_library")

function InitialiseItemLibraryCommand:initialise(...)
	local arg={...}
	
	return self
end

function InitialiseItemLibraryCommand:execute(...)
	local arg={...}

	local itemLibrary = _G.Game.Model:getModule("itemlibrary")

	-- INTERACT ITEM
	local interactItem = Item.new(
		"interact",
		"Interact",
		"interact",
		"interact",
		"interaction_cursor",
		"icons/icon_interact"
		
	)
	itemLibrary:addItem(interactItem)

	-- Shovel 001 ITEM
	local shovel01Item = Item.new(
		"shovel01",
		"Basic shovel",
		"dig",
		"tools",
		"grave_selection_cursor",
		"icons/icon_shovel"
	)
	itemLibrary:addItem(shovel01Item)

	-- Axe 001 ITEM
	local axe01Item = Item.new(
		"axe01",
		"Basic axe",
		"chop",
		"tools",
		"selection_cursor",
		"icons/icon_axe"
	)
	itemLibrary:addItem(axe01Item)

	-- Pick 001 ITEM
	local pick01Item = Item.new(
		"pick01",
		"Basic pick",
		"mine",
		"tools",
		"selection_cursor",
		"icons/icon_pick"
	)
	itemLibrary:addItem(pick01Item)


-- Pick 001 ITEM
	local basicgrave001Item = Item.new(
		"basicgrave001",
		"Basic Grave",
		"build_grave",
		"graves",
		"grave_selection_cursor",
		"icons/icon_basicgrave001"
	)
	itemLibrary:addItem(basicgrave001Item)

	

end


return InitialiseItemLibraryCommand