local command = require "core/commands/command"
local Grave = require "core/model/graves/grave"


local BuildGraveActionCommand = command.new("build_grave_action")

function BuildGraveActionCommand:initialise(...)
	local arg={...}
	
	return self
end

function BuildGraveActionCommand:execute(...)
	local arg={...}
	print("Build the grave")

	local newGrave = false
	local tileX = arg[1]
	local tileZ = arg[2]
	local map = _G.Game.Model:getModule("maps"):getCurrentMap()
	-- Work out if this is a new grave or digging deeper
	local currentGrave = map:getGrave(tileX, tileZ)
	if currentGrave == nil then
		-- keep digging
		print("Error")
		return
	end

	local dirtTileIndex = map:getTilesetTileByType("dirt")
	local graveTileIndex = map:getTilesetTileByType("basicgrave001")

	for i = 1, #currentGrave.positionGroup do
		local tilePos = currentGrave.positionGroup[i]
		local tileData = map:getTileData(tilePos.x, tilePos.y)
		tileData.ground = dirtTileIndex
		tileData.groundWalkable = true
		tileData.groundHeightOffset = 0

		if(i == 1)then
			tileData.props = graveTileIndex
			tileData.propsWalkable = false
		end
	end

	

	_G.Game.Command:execute("recalculate_waypoints_at_tile", tileX, tileZ)


	--[[currentGrave:lowerDepth(1)
	local dirtTileIndex = map:getTilesetTileByType("dirt")

	for i = 1, #currentGrave.positionGroup do
		local tilePos = currentGrave.positionGroup[i]
		local tileData = map:getTileData(tilePos.x, tilePos.y)
		tileData.ground = dirtTileIndex
		tileData.groundWalkable = false
		tileData.groundHeightOffset = currentGrave.graveDepth
	end

	if newGrave then

		_G.Game.Command:execute("recalculate_waypoints_at_tile", tileX, tileZ)
	end
	]]

end


return BuildGraveActionCommand