local command = require "core/commands/command"


local MineActionCommand = command.new("mine_action")

function MineActionCommand:initialise(...)
	local arg={...}
	
	return self
end

function MineActionCommand:execute(...)
	local arg={...}
	
	local tileX = arg[1]
	local tileZ = arg[2]
	local map = _G.Game.Model:getModule("maps"):getCurrent()
	
	local tile = map:getTilesetTileAtPosition(tileX, tileZ, "props")


	if(tile and tile.type == "stone") then
		local tileData = map:getTileData(tileX, tileZ)
		
		tileData.propsHitpoints = tileData.propsHitpoints -1
		print("Stone hitpoints "..tileData.propsHitpoints )
		if tileData.propsHitpoints <= 0 then
			print("destroy stone")
			tileData.props = 0
			tileData.propsWalkable = true

			_G.Game.Command:execute("recalculate_waypoints_at_tile", tileX, tileZ)
		end

	end
end


return MineActionCommand