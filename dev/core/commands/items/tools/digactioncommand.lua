local command = require "core/commands/command"
local Grave = require "core/model/graves/grave"


local DigActionCommand = command.new("dig_action")

function DigActionCommand:initialise(...)
	local arg={...}
	
	return self
end

function DigActionCommand:execute(...)
	local arg={...}
	local newGrave = false
	local tileX = arg[1]
	local tileZ = arg[2]
	local map = _G.Game.Model:getModule("maps"):getCurrentMap()
	-- Work out if this is a new grave or digging deeper
	local currentGrave = map:getGrave(tileX, tileZ)
	if currentGrave == nil then
		-- keep digging
		currentGrave = Grave.new(tileX, tileZ)
		map:addGrave(currentGrave, tileX, tileZ)
		newGrave = true
	end

	currentGrave:lowerDepth(1)
	local dirtTileIndex = map:getTilesetTileByType("dirt")

	for i = 1, #currentGrave.positionGroup do
		local tilePos = currentGrave.positionGroup[i]
		local tileData = map:getTileData(tilePos.x, tilePos.y)
		tileData.ground = dirtTileIndex
		tileData.groundWalkable = false
		tileData.groundHeightOffset = currentGrave.graveDepth
	end

	if newGrave then

		_G.Game.Command:execute("recalculate_waypoints_at_tile", tileX, tileZ)
	end


end


return DigActionCommand