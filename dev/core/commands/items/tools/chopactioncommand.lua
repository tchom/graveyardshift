local command = require "core/commands/command"


local ChopActionCommand = command.new("chop_action")

function ChopActionCommand:initialise(...)
	local arg={...}
	
	return self
end

function ChopActionCommand:execute(...)
	local arg={...}
	
	local tileX = arg[1]
	local tileZ = arg[2]
	local map = _G.Game.Model:getModule("maps"):getCurrent()
	
	local tile = map:getTilesetTileAtPosition(tileX, tileZ, "props")


	if(tile and tile.type == "tree") then
		local tileData = map:getTileData(tileX, tileZ)
		
		tileData.propsHitpoints = tileData.propsHitpoints -1
		if tileData.propsHitpoints <= 0 then
			tileData.props = 0
			tileData.propsWalkable = true

			_G.Game.Command:execute("recalculate_waypoints_at_tile", tileX, tileZ)
		end

	end

	


end


return ChopActionCommand