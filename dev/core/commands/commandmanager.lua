return {
	new = function()

		local commandManager = {
			commands = {}
		}

		function commandManager:registerCommand(command)
			self.commands[command.name] = command
		end

		function commandManager:execute(name, ...)
			assert(self.commands[name], "No such command as "..name)
			local arg={...}
			self.commands[name]:execute(unpack(arg))
		end

		return commandManager

	end

}