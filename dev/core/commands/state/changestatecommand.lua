local command = require "core/commands/command"

local ChangeStateCommand = command.new("change_state")

function ChangeStateCommand:initialise(...)
	local arg={...}
	
	self.gameState = arg[1]	
	return self
end

function ChangeStateCommand:execute(...)
	local arg={...}
	local newState = arg[1]


	self.gameState:gotoState(newState)

end


return ChangeStateCommand