return {
	new = function(_name)

		local command = {
			name = _name
		}

		function command:initialise(...)
			local arg={...}
			return self
		end

		function command:execute(...)
			local arg={...}
		end

		return command

	end

}