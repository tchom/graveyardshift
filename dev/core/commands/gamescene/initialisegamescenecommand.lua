local Component = require "core/entities/components/component"
local coms = require "core/entities/components/commoncomponents"
local common_systems = require "core/entities/systems/commonsystems"

local command = require "core/commands/command"

local InitialiseGameSceneCommand = command.new("initialise_game_scene")

function InitialiseGameSceneCommand:initialise(...)
	local arg={...}
	
	return self
end

function InitialiseGameSceneCommand:execute(...)
	local arg={...}
	local world = arg[1]
	print( "Initialise game scene" )


	local maps = _G.Game.Model:getModule("maps")
	local tileset = _G.Game.AssetsManager:getTileset("world_tiles")

	-- Register entity systems
	world:register(common_systems.new_renderer_system())
	world:register(common_systems.new_functional_system())
	world:register(common_systems.new_ai_system())
	world:register(common_systems.new_iso_tile_system())


	local worldAtlas = _G.Game.AssetsManager:getTextureAtlas("world_tiles")

	local tileWidth = GameSettings.REAL_TILE_WIDTH
	local tileHeight = GameSettings.REAL_TILE_HEIGHT
	local currentMap = _G.Game.Model:getModule("maps"):getCurrentMap()

	local tilesAcross = math.ceil( love.graphics.getWidth() / (tileWidth*0.5))
	local tilesDown = math.ceil( love.graphics.getHeight() / (tileHeight*0.5))

	for a = 0, tilesDown+10 do
		for b = -1, tilesAcross +2 do
			if not bit.band(b, 1) ~=  bit.band(a, 1) then 
		   		
		   		local x = math.floor( (a+b)/2 ) 
		   		local z = math.floor((a-b)/2)

			   world:create()
					:madd(coms.new_iso_tile_component(camera, x, z, "props", currentMap))
					:madd(coms.new_sprite("props/Forest_small_grass_01", worldAtlas, tileset.frames, 0, 0))
					:madd(coms.new_iso_transform(x, 0, z, GameSettings.REAL_TILE_WIDTH, true, 0.5))


				world:create()
					:madd(coms.new_iso_tile_component(camera, x, z, "ground", currentMap))
					:madd(coms.new_sprite("ground/ISO_Tile_Dirt_01_Grass_01", worldAtlas, tileset.frames))
					:madd(coms.new_iso_transform(x, GameSettings.GROUND_Y_OFFSET, z, GameSettings.REAL_TILE_WIDTH, false))
					:madd(coms.functional(
						function(self, dt)
							local body = self:get("iso_transform")
							--body.position.y = body.position.y + dt*0.2

						end))
			end
		end
	end


	local playerAtlas = _G.Game.AssetsManager:getTextureAtlas("player")
	local dummyTileset = {}
	dummyTileset["idle/alienGreen_stand"] = {
			framerate = 1,
			frames = {
				"idle/alienGreen_stand"
			}
		}

	local baseAiNode = 
	require("core/entities/components/ai/masternode"):new(
		require("core/entities/components/ai/mempriority"):new(
			require("core/entities/components/ai/memsequence"):new(
				require("core/entities/components/ai/sequence"):new(
					require("core/entities/components/ai/conditions/ismousedown"):new(),
					require("core/entities/components/ai/conditions/isinteractequipped"):new(),
					require("core/entities/components/ai/conditions/iswalkable"):new(),
					require("core/entities/components/ai/conditions/isportal"):new()
				), 
				
				require("core/entities/components/ai/actions/pathfinding/findpathtotarget"):new(),
				require("core/entities/components/ai/actions/pathfinding/movealongpath"):new(),
				require("core/entities/components/ai/actions/gotoportaltarget"):new()
			),
			require("core/entities/components/ai/memsequence"):new(
				require("core/entities/components/ai/sequence"):new(
					require("core/entities/components/ai/conditions/ismousedown"):new(),
					require("core/entities/components/ai/conditions/isinteractequipped"):new(),
					require("core/entities/components/ai/conditions/iswalkable"):new()
				), 
				
				require("core/entities/components/ai/actions/pathfinding/findpathtotarget"):new(),
				require("core/entities/components/ai/actions/pathfinding/movealongpath"):new()
			),

			require("core/entities/components/ai/memsequence"):new(
				require("core/entities/components/ai/sequence"):new(
					require("core/entities/components/ai/conditions/ismousedown"):new(),
					require("core/entities/components/ai/conditions/isresource"):new()
				),
				require("core/entities/components/ai/actions/pathfinding/findpathtoadjacent"):new(),
				require("core/entities/components/ai/actions/pathfinding/movealongpath"):new(),
				require("core/entities/components/ai/actions/performtoolaction"):new(),
				require("core/entities/components/ai/actions/wait"):new(0.3)
			),

			require("core/entities/components/ai/memsequence"):new(
				require("core/entities/components/ai/sequence"):new(
					require("core/entities/components/ai/conditions/ismousedown"):new(),
					require("core/entities/components/ai/conditions/isbuildgraveequipped"):new(),
					require("core/entities/components/ai/conditions/isvalidgravespace"):new(),
					require("core/entities/components/ai/conditions/isemptygrave"):new()
				),
				require("core/entities/components/ai/actions/pathfinding/findpathtoadjacent"):new(),
				require("core/entities/components/ai/actions/pathfinding/movealongpath"):new(),
				require("core/entities/components/ai/actions/performbuildgrave"):new(),
				require("core/entities/components/ai/actions/wait"):new(0.3)
			),

			require("core/entities/components/ai/memsequence"):new(
				require("core/entities/components/ai/sequence"):new(
					require("core/entities/components/ai/conditions/ismousedown"):new(),
					require("core/entities/components/ai/conditions/isvalidgravespace"):new()
				),
				require("core/entities/components/ai/actions/pathfinding/findpathtoadjacent"):new(),
				require("core/entities/components/ai/actions/pathfinding/movealongpath"):new(),
				require("core/entities/components/ai/actions/performtoolaction"):new(),
				require("core/entities/components/ai/actions/wait"):new(0.3)
			)
		)
	)



	self.player = world:create()
		:madd(coms.new_sprite("idle/alienGreen_stand", playerAtlas, dummyTileset, 0, -32))
		:madd(coms.new_iso_transform(0, 0, 0, GameSettings.REAL_TILE_WIDTH, true, 0.5))
		:madd(coms.new_player_component())
		:madd(coms.new_ai_component(baseAiNode))
		:madd(coms.functional(
						function(self, dt)
							--[[local body = self:get("iso_transform")


						if love.mouse.isDown(1) then
							local mouseX, mouseY = love.mouse.getPosition( )
      						local screenPos = Vec2.new((mouseX + camera.position.x)/GameSettings.TILE_SIZE, (mouseY + camera.position.y)/GameSettings.TILE_SIZE)
							local isoScreenPos = IsoUtils.screenToIso(screenPos)
							body.position = isoScreenPos

						end]]

						end))
	
end


return InitialiseGameSceneCommand