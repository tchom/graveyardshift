local command = require "core/commands/command"

local CreateTextureAtlasCommand = command.new("create_texture_atlas")

function CreateTextureAtlasCommand:initialise(...)
	local arg={...}
	
	return self
end

function CreateTextureAtlasCommand:execute(...)
	local arg={...}

	local atlasName = arg[1]
	local atlasDataPath = arg[2]
	local atlasImagePath = arg[3]

	local newAtlas =  AtlasImporter.loadAtlasTexturePacker(atlasDataPath, atlasImagePath)

	_G.Game.AssetsManager:addTextureAtlas(atlasName, newAtlas)

end


return CreateTextureAtlasCommand