local Tileset = require "core/assets/tileset"
local Tile = require "core/assets/tile"
local HitResource = require "core/model/tiles/hitresource"
local command = require "core/commands/command"

local CreateTilesetFromTiledCommand = command.new("create_tileset_from_tiled")

function CreateTilesetFromTiledCommand:initialise(...)
	local arg={...}

	self.assetManager = arg[1]
	return self
end

function CreateTilesetFromTiledCommand:execute(...)
	local arg={...}
	local tilesetName = arg[1]
	local tiledMap = arg[2]
	local pathClip = arg[3]


	if not self.assetManager:hasTileset(tilesetName) then
		local tileset = Tileset.new(tilesetName)

		for i = 1, #tiledMap.tilesets do
			local ts = tiledMap.tilesets[i]
			

			for j = 1, #ts.tiles do
				local tiledTile = ts.tiles[j]

				local tileName = string.match(tiledTile.image, pathClip.."([^!]*).png")

				local resourcesOnHitPackage = self:createHitResource(tiledTile.properties.resourcesOnHit)
				local resourcesOnDestroyPackage = self:createHitResource(tiledTile.properties.resourcesOnDestroy)
				local tile = Tile.new(tileName, tiledTile.type, tiledTile.properties.walkable, tiledTile.properties.hitpoints,
					resourcesOnHitPackage, resourcesOnDestroyPackage,  tiledTile.properties.heightOffset or 0)
				tileset.tiles[ts.firstgid + tiledTile.id] = tile
				tileset.frames[tileName] = {
					framerate = 1,
					frames = {
						tileName
					}
				}

			end
		end


		self.assetManager:addTileset(tilesetName, tileset)
	end
end


function CreateTilesetFromTiledCommand:createHitResource(hitResourcesString)
	if not(hitResourcesString) then
		return nil
	end

	local resoursesDataString, chance = string.match(hitResourcesString, "(.*)%|(.*)")
	local hitResource = HitResource.new(tonumber(chance))

	for resourceString in string.gmatch(resoursesDataString, '([^,]+)') do
	    local resourseName, value = string.match(hitResourcesString, "(.*)%.(.*)")

	    hitResource:addResource(resourseName, value)
	end

	
	return hitResource
	
end


return CreateTilesetFromTiledCommand