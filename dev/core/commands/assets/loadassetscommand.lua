local command = require "core/commands/command"

local LoadAssetsCommand = command.new("load_assets")

function LoadAssetsCommand:initialise(...)
	local arg={...}
	
	return self
end

function LoadAssetsCommand:execute(...)
	local arg={...}

	-- MACRO COMMAND FOR LAUNCHING GAME RESOURCES
	--texture atlases
	_G.Game.Command:execute("create_texture_atlas", "world_tiles", "assets/tilesheets/world_tiles", "assets/tilesheets/world_tiles.png")
	_G.Game.Command:execute("create_texture_atlas", "player", "assets/tilesheets/player", "assets/tilesheets/player.png")
	_G.Game.Command:execute("create_texture_atlas", "game_scene_ui", "assets/tilesheets/game_scene_ui", "assets/tilesheets/game_scene_ui.png")
	--shader
	_G.Game.Command:execute("create_shader", "light-shader",  love.graphics.newShader(require( "assets/shaders/lighting" ).code))



end


return LoadAssetsCommand