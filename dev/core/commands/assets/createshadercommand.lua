local command = require "core/commands/command"

local CreateShaderCommand = command.new("create_shader")

function CreateShaderCommand:initialise(...)
	local arg={...}
	
	return self
end

function CreateShaderCommand:execute(...)
	local arg={...}
	
	local shaderName = arg[1]
	local shader = arg[2]
	
	

	_G.Game.AssetsManager:addShader(shaderName, shader)

end


return CreateShaderCommand