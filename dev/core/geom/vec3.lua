return{
	new = function (x, y, z)
		
		local vec3 = {
			x = x or 0,
			y = y or 0,
			z = z or 0
		}

		
		function vec3:clone()
			local orig_type = type(self)
		    local copy
		    if orig_type == 'table' then
		        copy = {}
		        for orig_key, orig_value in pairs(self) do
		            copy[orig_key] = orig_value
		        end
		    else -- number, string, boolean, etc
		        copy = self
		    end
		    return copy
		end


		return vec3
	end
}