return{
	new = function (x, y)
		
		local vec2 = {
			x = x or 0,
			y = y or 0
		}

		function vec2:clone()
			local orig_type = type(self)
		    local copy
		    if orig_type == 'table' then
		        copy = {}
		        for orig_key, orig_value in pairs(self) do
		            copy[orig_key] = orig_value
		        end
		    else -- number, string, boolean, etc
		        copy = self
		    end
		    return copy
		end

		
		return vec2
	end
}