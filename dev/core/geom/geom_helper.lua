local Vec3 = require "core/geom/vec3"
local Vec2 = require "core/geom/vec2"

local Y_CORRECT = math.cos( -math.pi/6 )*math.sqrt( 2 )


return{

	isoToScreen = function(pos)
		local screenX = pos.x - pos.z
		local screenY = pos.y * Y_CORRECT + (pos.x + pos.z) * 0.5
		

		return Vec2.new(screenX, screenY)
	end,

	screenToIso = function(point)
		local xpos = point.y + point.x/2
		local ypos = 0
		local zpos = point.y - point.x/2
		

		return Vec3.new(xpos, ypos, zpos)
	end,

	calculateBreshamLine = function(x0, y0, x1, y1)
		local dx = x1 - x0
		local dy = y1 - y0
		local nx = math.abs( dx )
		local ny = math.abs( dy )

		local sign_x = dx > 0 and 1 or -1 
		local sign_y = dy > 0 and 1 or -1 

		local p = Vec2.new(x0, y0)
		local points = {}

		local ix= 0
		local iy= 0
	
		while ix < nx or iy < ny do
			if ((0.5+ix) / nx) < ((0.5+iy)/ny) then 
				-- next step is horizontal
	            p.x =  p.x + sign_x
	            ix = ix + 1
			else
				 -- next step is vertical
	            p.y =  p.y + sign_y
	            iy = iy + 1
			end
			table.insert(points, Vec3.new(p.x, 0, p.y))
		end

		return points

	end,

	checkValidPath = function(path, map)
		local legalPath = true

		for i = 1, #path do
			local pathpoint = path[i]
			if not map:isWalkable(pathpoint.x, pathpoint.z) then
				legalPath = false
			end
		end

		return legalPath
	end


}